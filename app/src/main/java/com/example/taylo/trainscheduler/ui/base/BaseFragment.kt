package com.example.taylo.trainscheduler.ui.base

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.example.taylo.trainscheduler.app.MyApplication
import com.example.taylo.trainscheduler.di.component.ActivityComponent
import com.example.taylo.trainscheduler.di.component.DaggerFragmentComponent

import com.example.taylo.trainscheduler.di.component.FragmentComponent
import com.example.taylo.trainscheduler.di.module.FragmentModule

abstract class BaseFragment: Fragment(), IBaseView {

    private lateinit var fragmentComponent: FragmentComponent

    var baseActivity:BaseActivity? = null
        private set


    val isNetworkConnected:Boolean
        get() = if (baseActivity != null) {
            baseActivity!!.networkConnected()
        } else false

    val activityComponent: ActivityComponent?
        get() {
            return if (baseActivity != null) {
                baseActivity!!.getActivityComponent()
            } else null
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)

        fragmentComponent = DaggerFragmentComponent.builder()
                .fragmentModule(FragmentModule(this))
                .applicationComponent((activity!!.application as MyApplication).getComponent())
                .build()
    }


    override fun onViewCreated(view: View, savedInstanceState:Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is BaseActivity) {
            val activity = context as BaseActivity?
            this.baseActivity = activity
            activity!!.onFragmentAttached()
        }
    }

    override fun onDetach() {
        baseActivity = null
        super.onDetach()
    }



    fun hideKeyboard() {
        if (baseActivity != null)
        {
            baseActivity!!.hideKeyboard()
        }
    }

    fun getFragmentComponent(): FragmentComponent {
        return fragmentComponent
    }

    fun openActivityOnTokenExpire() {
        if (baseActivity != null)
        {
            baseActivity!!.openActivityOnTokenExpire()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    interface Callback {

        fun onFragmentAttached()

        fun onFragmentDetached(tag:String)

    }

}
