package com.example.taylo.trainscheduler.ui.activity.setting

import com.example.taylo.trainscheduler.ui.base.IBasePresenter


interface ISettingPresenter: IBasePresenter<ISettingView> {
    fun refresh()
}