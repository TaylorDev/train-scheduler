package com.example.taylo.trainscheduler.ui.activity.setting

import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.base.IBaseView


interface ISettingView: IBaseView {
    fun onProvideResource(resource: Resource<MutableList<Station>>)
}