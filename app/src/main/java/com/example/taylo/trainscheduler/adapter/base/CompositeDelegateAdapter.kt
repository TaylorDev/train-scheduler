package com.example.taylo.trainscheduler.adapter.base

import android.support.v7.widget.RecyclerView
import android.util.SparseArray
import android.view.ViewGroup

class CompositeDelegateAdapter
protected constructor(protected val typeToAdapterMap: SparseArray<IDelegateAdapter<*>>) :
        RecyclerView.Adapter<KViewHolder>() {

    private var data: MutableList<Any> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KViewHolder {
        return typeToAdapterMap.get(viewType).createViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: KViewHolder, position: Int) {
        val delegateAdapter = typeToAdapterMap.get(getItemViewType(position))
        if (delegateAdapter != null) {
            delegateAdapter.bindViewHolder(holder, data[position], position)
        } else {
            throw NullPointerException("can not find adapter for position " + position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        for (i in FIRST_VIEW_TYPE until typeToAdapterMap.size()) {
            val delegate = typeToAdapterMap.valueAt(i)
            if (delegate.isForViewType(data, position)) {
                return typeToAdapterMap.keyAt(i)
            }
        }
        throw NullPointerException("Can not get viewType for position " + position)
    }

    override fun onViewRecycled(holder: KViewHolder) {
        typeToAdapterMap.get(holder.itemViewType).recycled(holder)
    }






    fun swapData(data: List<Any>?) {
        if (data != null) {
            this.data = data as MutableList<Any>
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class Builder {

        private var count: Int = 0
        private val typeToAdapterMap: SparseArray<IDelegateAdapter<*>> = SparseArray()

        fun add(delegateAdapter: IDelegateAdapter<*>): Builder {
            typeToAdapterMap.put(count++, delegateAdapter)
            return this
        }

        fun build(): CompositeDelegateAdapter {
            if (count == 0) {
                throw IllegalArgumentException("Register at least one adapter")
            }
            return CompositeDelegateAdapter(typeToAdapterMap)
        }
    }

    companion object {
        private val FIRST_VIEW_TYPE = 0
    }

    fun removeItem(position: Int) {
        data.removeAt(position)
        notifyItemRemoved(position)
    }

}