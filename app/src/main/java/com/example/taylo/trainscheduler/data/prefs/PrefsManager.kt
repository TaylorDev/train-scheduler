package com.example.taylo.trainscheduler.data.prefs

interface PrefsManager {
    fun getLastUpdatedTime(): Long
    fun saveLastUpdatedTime(time: Long)
    fun isFirstLaunch(): Boolean
    fun saveFirstLaunch(isFirstLaunch: Boolean)
    fun getLastUpdatedTimeWithFirebase(): Long
    fun saveLastUpdatedTimeWithFirebase(time: Long)
}
