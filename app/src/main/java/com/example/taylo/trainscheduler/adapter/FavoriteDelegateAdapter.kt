package com.example.taylo.trainscheduler.adapter

import android.support.v4.content.ContextCompat
import com.example.taylo.trainscheduler.R
import com.example.taylo.trainscheduler.adapter.base.BaseDelegateAdapter
import com.example.taylo.trainscheduler.adapter.base.KViewHolder
import com.example.taylo.trainscheduler.model.station.Station
import kotlinx.android.synthetic.main.list_item_favorite.view.*

class FavoriteDelegateAdapter(private val callback: Callback) : BaseDelegateAdapter<Station>() {

    override fun isForViewType(items: List<Any>, position: Int): Boolean {
        return items[position] is Station
    }

    override fun onBindViewHolder(holder: KViewHolder, item: Station, position: Int) {
        holder.containerView.nameTextView.text = item.nameStation
        holder.containerView.setOnLongClickListener {
            callback.onItemLongClick(item, holder.adapterPosition)
            true
        }
        holder.containerView.setOnClickListener {
            callback.onItemClick(item, holder.adapterPosition)
        }

        when(item.findState()) {
            Station.STATE.OPEN -> {
                holder.containerView.isClosedTextView.text = "Сейчас открыта"
                holder.containerView.imageView.setColorFilter(ContextCompat.getColor(holder.containerView.context, R.color.green))
                holder.containerView.closeTimeTextView.text = "Закроется через " + item.getNextTime(true)
            }
            Station.STATE.CLOSED -> {
                holder.containerView.isClosedTextView.text = "Сейчас закрыта"
                holder.containerView.imageView.setColorFilter(ContextCompat.getColor(holder.containerView.context, R.color.red))
                holder.containerView.closeTimeTextView.text = "Откроется через " + item.getNextTime(false)
            }
            Station.STATE.UNKNOWN -> {
                holder.containerView.isClosedTextView.text = "Нет информации"
                holder.containerView.imageView.setColorFilter(ContextCompat.getColor(holder.containerView.context, R.color.black))
                holder.containerView.closeTimeTextView.text = "Нет информации"
            }
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.list_item_favorite
    }

    interface Callback {
        fun onItemLongClick(station: Station, position: Int)
        fun onItemClick(station: Station, position: Int)
    }
}
