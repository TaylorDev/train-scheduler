package com.example.taylo.trainscheduler.ui.activity.search

import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.base.IBaseView

interface SearchMvpView : IBaseView {
    fun onProvideResource(resource: Resource<MutableList<Station>>)
}
