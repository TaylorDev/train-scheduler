package com.example.taylo.trainscheduler.model

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
