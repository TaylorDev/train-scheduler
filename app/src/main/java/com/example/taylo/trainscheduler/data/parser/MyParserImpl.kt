package com.example.taylo.trainscheduler.data.parser

import android.content.Context
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.model.station.StationSchedule
import io.reactivex.Observable
import io.reactivex.Single
import org.jsoup.Jsoup
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MyParserImpl
@Inject constructor(private val context: Context) : MyParser {

    override fun parse(list: MutableList<Station>): Single<MutableList<Station>> {
        return Observable.fromIterable(list)
                .map { item -> parseForItem(item) }
                .toList()
    }

    private fun parseForItem(item: Station): Station {
        item.scheduleList = mutableListOf()
        if (item.url == null) {
            return item
        }

        val connection = Jsoup.connect(item.url)
                .userAgent("Mozilla/5.0 (Linux; Android 8.1; SM-G930V Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.125 Mobile Safari/537.36")
                .header("Accept-Language", "ru")
        val doc = connection.get()
        val data = doc.body()
        Timber.d("Data $data")
        val routersBody = doc.getElementsByClass("b-routers__body")
        if (routersBody.size != 0) {
            val stationRows = routersBody[0].getElementsByTag("tr")
            for (stationRow in stationRows) {
                val times = stationRow.getElementsByClass("b-routers__time_type_departure")
                val timesInPath = stationRow.getElementsByClass("b-routers__time_type_in-path")

                if (times.size != 0 && timesInPath.size != 0) {
                    val except = timesInPath[0].getElementsByClass("b-timetable__except")
                    var exceptStr = ""
                    if (except.size != 0) {
                        exceptStr = except[0].ownText()
                    }
                    val schedule = StationSchedule.create(times[0].ownText(), timesInPath[0].ownText() + " " + exceptStr)
                    item.scheduleList.add(schedule)
                }
            }
        }
        return item
    }
}
