package com.example.taylo.trainscheduler.di.component

import com.example.taylo.trainscheduler.di.FragmentScope
import com.example.taylo.trainscheduler.di.module.FragmentModule
import dagger.Component

@FragmentScope
@Component(dependencies = [(ApplicationComponent::class)], modules = [(FragmentModule::class)])
interface FragmentComponent{

}