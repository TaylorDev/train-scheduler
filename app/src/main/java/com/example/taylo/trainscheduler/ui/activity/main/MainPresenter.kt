package com.example.taylo.trainscheduler.ui.activity.main

import com.example.taylo.trainscheduler.data.DataManager
import com.example.taylo.trainscheduler.ui.base.BasePresenter
import com.example.taylo.trainscheduler.utils.rx.ISchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MainPresenter
@Inject
constructor(schedulerProvider: ISchedulerProvider,
            dataManager: DataManager,
            compositeDisposable: CompositeDisposable) : BasePresenter<IMainView>(schedulerProvider , dataManager, compositeDisposable) , IMainPresenter {

    override fun onAttach(view: IMainView) {
        super.onAttach(view)
        view.showSyncing(dataManager.isSyncing())
    }
}