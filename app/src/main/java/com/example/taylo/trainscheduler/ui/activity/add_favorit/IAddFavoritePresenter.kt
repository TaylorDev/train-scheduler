package com.example.taylo.trainscheduler.ui.activity.add_favorit

import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.base.IBasePresenter


interface IAddFavoritePresenter: IBasePresenter<IAddFavoriteView> {
    fun save(stations: MutableList<Station>)
    fun refresh()
}