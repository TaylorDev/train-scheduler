package com.example.taylo.trainscheduler.model.station

import android.text.TextUtils
import io.objectbox.annotation.Transient
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

data class StationSchedule(
        var isEveryDay: Boolean = false,
        var isWeekend: Boolean = false,
        var isWeekdays: Boolean = false,
        var onlyDates: MutableList<String> = ArrayList(),
        var exceptDates: MutableList<String> = ArrayList(),
        var fromDates: MutableList<String> = ArrayList(),
        var byDates: MutableList<String> = ArrayList(),
        var time: String = "",

        @Transient
        var closeTimeMillis: Long = 0,
        @Transient
        var beforeCloseTimeMillis: Long = 0,
        @Transient
        var afterCloseTimeMillis: Long = 0,

        @Transient
        private var closedTimeIntervalFormatted: String? = null): Serializable {

    fun computeTimeStringInterval(): String {
        if (closedTimeIntervalFormatted === null) {
            val dateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
            val beforeCloseDate = Date(beforeCloseTimeMillis)
            val afterCloseDate = Date(afterCloseTimeMillis)
            closedTimeIntervalFormatted = dateFormat.format(beforeCloseDate) + " - " + dateFormat.format(afterCloseDate)
//            closedTimeIntervalFormatted = dateFormat.format(closeTimeMillis)
        }
        return closedTimeIntervalFormatted!!
    }

    companion object {

        private const val EVERY_DAY = "ежедневно"
        private const val WEEKEND = "по выходным"
        private const val WEEKDAYS = "по будням"
        private const val ONLY = "только"
        private const val EXCEPT = "кроме"
        private const val FROM = "с"
        private const val BY = "по"

        private const val SUN = "вс"
        private const val MON = "пн"
        private const val TUE = "вт"
        private const val WED = "ср"
        private const val THR = "чт"
        private const val FRI = "пт"
        private const val SAT = "сб"

        private const val JAN = "января"
        private const val FEB = "февраля"
        private const val MAR = "марта"
        private const val APR = "апреля"
        private const val MAY = "мая"
        private const val JUN = "июня"
        private const val JUL = "июля"
        private const val AUG = "августа"
        private const val SEP = "сентября"
        private const val OCT = "октября"
        private const val NOV = "ноября"
        private const val DEC = "декабря"

        private const val DAY_MILLIS = 86400000

        fun create(time: String, conditions: String): StationSchedule {
            var string = conditions
            val stationSchedule = StationSchedule()
            stationSchedule.time = time

            string = string.replace("остановки: кроме", "").replace(",", "")
            val array = string.split("\\s+".toRegex())
            var currentKeyWord = ""

            for (i in array.indices) {
                val str = array[i]
                if (TextUtils.isEmpty(str)) {
                    continue
                }
                var collection: MutableList<String>? = null

                when (currentKeyWord) {
                    ONLY -> collection = stationSchedule.onlyDates
                    EXCEPT -> collection = stationSchedule.exceptDates
                    FROM -> collection = stationSchedule.fromDates
                    BY -> collection = stationSchedule.byDates
                }

                if (addToCollection(collection, str)) {
                    continue
                }

                when (str) {
                    SUN, MON, TUE, WED, THR, FRI, SAT -> stationSchedule.onlyDates.add(str)
                    EVERY_DAY -> stationSchedule.isEveryDay = true
                    BY -> {
                        if ((i + 1) < array.size) {
                            val nextStr = array[i + 1]
                            when(BY + " " + nextStr) {
                                WEEKEND -> stationSchedule.isWeekend = true
                                WEEKDAYS -> stationSchedule.isWeekdays = true
                            }
                        }
                    }
                    else -> currentKeyWord = str
                }
            }

            return stationSchedule
        }

        private fun addToCollection(dates: MutableList<String>?, str: String): Boolean {
            if (dates == null || TextUtils.isEmpty(str)) {
                return false
            }
            if (TextUtils.isDigitsOnly(str)) {
                dates.add(str)
                return true
            } else {
                var month: String? = null
                when (str) {
                    JAN -> month = "0"
                    FEB -> month = "1"
                    MAR -> month = "2"
                    APR -> month = "3"
                    MAY -> month = "4"
                    JUN -> month = "5"
                    JUL -> month = "6"
                    AUG -> month = "7"
                    SEP -> month = "8"
                    OCT -> month = "9"
                    NOV -> month = "10"
                    DEC -> month = "11"
                }
                if (month != null) {
                    addMonthToDates(dates, month)
                    return true
                }

                when (str) {
                    SUN, MON, TUE, WED, THR, FRI, SAT -> {
                        dates.add(str)
                        return true
                    }
                }
            }
            return false
        }

        private fun addMonthToDates(dates: MutableList<String>, month: String) {
            for (i in dates.indices.reversed()) {
                var date = dates[i]
                if (TextUtils.isDigitsOnly(date)) {
                    date = date + " " + month
                    dates.removeAt(i)
                    dates.add(i, date)
                }
            }
        }













        private var currentCalendar: Calendar? = null
        private var tempCalendar: Calendar? = null
        private var currentMonthNumber = ""
        private var currentDayOfMonth = ""
        private var currentDayName = ""
        private var currentDayOfWeek = -1

        fun computeDetail(scheduleList: MutableList<StationSchedule>) {

            currentCalendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"))
            currentCalendar!!.set(Calendar.SECOND, 0)
            currentCalendar!!.set(Calendar.MILLISECOND, 0)
            tempCalendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"))
            tempCalendar!!.set(Calendar.SECOND, 0)
            tempCalendar!!.set(Calendar.MILLISECOND, 0)
            currentMonthNumber = currentCalendar!!.get(Calendar.MONTH).toString()
            currentDayOfMonth = currentCalendar!!.get(Calendar.DAY_OF_MONTH).toString()
            currentDayName = currentCalendar!!.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault())
            currentDayOfWeek = currentCalendar!!.get(Calendar.DAY_OF_WEEK)

            for (i in scheduleList.indices.reversed()) {
                val item = scheduleList[i]
                if (needToRemove(item)) {
                    scheduleList.removeAt(i)
                    continue
                }

                val timeStr = item.time
                if (timeStr.length == 5) { // 00:00
                    val hour_mitute = timeStr.split(":")
                    if (hour_mitute.size == 2) {
                        tempCalendar!!.set(Calendar.HOUR_OF_DAY, hour_mitute[0].toInt())
                        tempCalendar!!.set(Calendar.MINUTE, hour_mitute[1].toInt())
                        item.closeTimeMillis = tempCalendar!!.timeInMillis

                        tempCalendar!!.add(Calendar.MINUTE, -1)
                        item.beforeCloseTimeMillis = tempCalendar!!.timeInMillis
                        tempCalendar!!.add(Calendar.MINUTE, 2)
                        item.afterCloseTimeMillis = tempCalendar!!.timeInMillis
                    }
                } else if (timeStr.length == 11) {// 00:00-01:00
                    val closeOpenTime = timeStr.split("-")
                    if (closeOpenTime.size == 2) {
                        val open_hour_mitute = closeOpenTime[0].split(":")
                        if (open_hour_mitute.size == 2) {
                            tempCalendar!!.set(Calendar.HOUR_OF_DAY, open_hour_mitute[0].toInt())
                            tempCalendar!!.set(Calendar.MINUTE, open_hour_mitute[1].toInt())
                            item.closeTimeMillis = tempCalendar!!.timeInMillis
                            item.beforeCloseTimeMillis = tempCalendar!!.timeInMillis
                        }
                        val close_hour_minute = closeOpenTime[1].split(":")
                        if (close_hour_minute.size == 2) {
                            tempCalendar!!.set(Calendar.HOUR_OF_DAY, close_hour_minute[0].toInt())
                            tempCalendar!!.set(Calendar.MINUTE, close_hour_minute[1].toInt())
                            item.closeTimeMillis = tempCalendar!!.timeInMillis
                            item.afterCloseTimeMillis = tempCalendar!!.timeInMillis
                        }
                    }
                }

            }
            sort(scheduleList)
            reorderFromNow(scheduleList)
        }

        private fun parseTime(time: String) {

        }

        private fun sort(scheduleList: MutableList<StationSchedule>) {
            Collections.sort(scheduleList, Comparator { o1, o2 ->
                if (o1.beforeCloseTimeMillis < o2.beforeCloseTimeMillis) {
                    return@Comparator -1
                } else if (o1.beforeCloseTimeMillis > o2.beforeCloseTimeMillis) {
                    return@Comparator 1
                }
                0
            })
        }

        private fun reorderFromNow(scheduleList: MutableList<StationSchedule>) {
            //val calendar = Calendar.getInstance()
            val currentTimeMillis = currentCalendar!!.timeInMillis
            val tempList = mutableListOf<StationSchedule>()
            for (i in scheduleList.indices.reversed()) {
                val schedule = scheduleList[i]
                //calendar.timeInMillis = schedule.closeTimeMillis
                if (schedule.beforeCloseTimeMillis < currentTimeMillis) {
                    schedule.closeTimeMillis += DAY_MILLIS
                    schedule.beforeCloseTimeMillis += DAY_MILLIS
                    schedule.afterCloseTimeMillis += DAY_MILLIS
                    tempList.add(0, schedule)
                    scheduleList.removeAt(i)
                }
            }

            scheduleList += tempList
//            val cal = Calendar.getInstance()
//            for(item in scheduleList) {
//                cal.timeInMillis = item.closeTimeMillis
//
//            }
        }

        private fun needToRemove(item: StationSchedule): Boolean {
            if (item.isWeekdays && !checkIsWeekDay()) {
                return true
            } else if (item.isWeekend && !checkIsWeekend()) {
                return true
            }

            if (!checkFromDates(item.fromDates)) {
                return true
            }
            if (!checkByDates(item.byDates)) {
                return true
            }

            if (!checkOnlyDates(item.onlyDates)) {
                return true
            }
            if (!checkExceptDates(item.exceptDates)) {
                return true
            }

            return false
        }

        private fun checkFromDates(list: MutableList<String>): Boolean {
            for(item in list) {
                return isAfterDate(item)
            }
            return true
        }

        private fun checkByDates(list: MutableList<String>): Boolean {
            for(item in list) {
                return isBeforeDate(item)
            }
            return true
        }

        private fun checkOnlyDates(list: MutableList<String>): Boolean {
            if (list.size == 0) {
                return true
            }
            return list.any { isSameDate(it) }
        }

        private fun checkExceptDates(list: MutableList<String>): Boolean {
            if (list.size == 0) {
                return true
            }
            return list.none { isSameDate(it) }
        }

        private fun isSameDate(dateStr: String): Boolean {
            if (dateStr.length == 2) { //e.g. 'сб', 'вс'
                return dateStr == currentDayName
            } else if (dateStr.length == 5)  { //e.g. '25 11'
                val day_Month = dateStr.split("\\s+".toRegex())
                if (day_Month.size == 2) {
                    return (day_Month[0] == currentDayOfMonth && day_Month[1] == currentMonthNumber)
                }
            }
            return false
        }

        private fun isBeforeDate(dateStr: String): Boolean {
            if (dateStr.length == 5)  { //e.g. '25 11'
                val day_Month = dateStr.split("\\s+".toRegex())
                if (day_Month.size == 2) {
                    return (currentMonthNumber < day_Month[1] ||
                            (currentMonthNumber == day_Month[1] && currentDayOfMonth <= day_Month[0]))
                }
            }
            return false
        }

        private fun isAfterDate(dateStr: String): Boolean {
            if (dateStr.length == 5)  { //e.g. '25 11'
                val day_Month = dateStr.split("\\s+".toRegex())
                if (day_Month.size == 2) {
                    return (currentMonthNumber > day_Month[1] ||
                            (currentMonthNumber == day_Month[1] && currentDayOfMonth >= day_Month[0]))
                }
            }
            return false
        }


        private fun checkIsWeekend(): Boolean {
            return currentDayOfWeek == Calendar.SATURDAY ||
                    currentDayOfWeek == Calendar.SUNDAY
        }

        private fun checkIsWeekDay(): Boolean {
            return !(currentDayOfWeek == Calendar.SATURDAY ||
                    currentDayOfWeek == Calendar.SUNDAY)
        }
    }


}