package com.example.taylo.trainscheduler.di.module

import android.content.Context
import android.content.SharedPreferences
import com.example.taylo.trainscheduler.app.MyApplication
import com.example.taylo.trainscheduler.data.DataManager
import com.example.taylo.trainscheduler.data.DataManagerImpl
import com.example.taylo.trainscheduler.data.api.API
import com.example.taylo.trainscheduler.data.database.DbManager
import com.example.taylo.trainscheduler.data.database.DbManagerImpl
import com.example.taylo.trainscheduler.data.parser.MyParser
import com.example.taylo.trainscheduler.data.parser.MyParserImpl
import com.example.taylo.trainscheduler.data.prefs.PrefsManager
import com.example.taylo.trainscheduler.data.prefs.PrefsManagerImpl
import com.example.taylo.trainscheduler.model.station.MyObjectBox
import com.example.taylo.trainscheduler.utils.AppConstants
import com.example.taylo.trainscheduler.utils.rx.AppSchedulerProvider
import com.example.taylo.trainscheduler.utils.rx.ISchedulerProvider
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import io.objectbox.BoxStore
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: MyApplication) {

    @Provides
    @Singleton
    fun provideApplicationContext() : Context = application

    @Provides
    @Singleton
    fun provideApplication() : MyApplication = application

    @Provides
    @Singleton
    fun provideDataManager(dataManager: DataManagerImpl) : DataManager {
        return dataManager
    }

    @Provides
    @Singleton
    fun provideRetrofitDefaultConfig() : API {
        val rxAdapter = RxJava2CallAdapterFactory.create()
        val json = GsonBuilder().create()
        val okHttpClient = OkHttpClient.Builder()
                .addNetworkInterceptor(StethoInterceptor())
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(json))
                .addCallAdapterFactory(rxAdapter)
                .build()
        return retrofit.create(API::class.java)
    }

    @Provides
    @Singleton
    fun provideSchedulerProvider(): ISchedulerProvider {
        return AppSchedulerProvider()
    }

    @Provides
    @Singleton
    fun provideParser(parser: MyParserImpl): MyParser = parser

    @Provides
    @Singleton
    fun providePrefsManager(manager: PrefsManagerImpl): PrefsManager = manager

    @Provides
    @Singleton
    fun provideSharedPrefs(context: Context): SharedPreferences = context.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun provideBoxStore(context: Context): BoxStore {
        return MyObjectBox.builder().androidContext(context).build()
    }

    @Provides
    @Singleton
    fun provideDbManage(dbManager: DbManagerImpl): DbManager = dbManager

    /*@Provides
    @Singleton
    fun provideReminderManager(reminderManager: ReminderManager): ReminderManager = reminderManager*/
}