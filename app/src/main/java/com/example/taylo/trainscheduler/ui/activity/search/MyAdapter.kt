package com.example.taylo.trainscheduler.ui.activity.search

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import com.example.taylo.trainscheduler.R
import com.example.taylo.trainscheduler.model.station.Station
import com.google.android.gms.maps.model.LatLng

class MyAdapter(context: Context, val callback: Callback, resource: Int, val mObjects: MutableList<Station>) : ArrayAdapter<Station>(context, resource, mObjects) {

    private var mOriginalValues = ArrayList(mObjects)

    private val mLock = Any()

    fun setData(list: MutableList<Station>) {
        mObjects.clear()
        mObjects.addAll(list)
        mOriginalValues = ArrayList(mObjects)
        notifyDataSetChanged()
        dispatchMarkers()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        if (convertView == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.support_simple_spinner_dropdown_item, parent, false)
        }

        val stationItem = mObjects[position]
        val textView = view as TextView
        textView.text = stationItem.nameStation
        textView.setOnClickListener { callback.onStationClicked(mObjects[position].id) }

        return textView
    }

    override fun getFilter(): Filter {
        return myFilter
    }






    private val myFilter = object : Filter() {

        override fun performFiltering(prefix: CharSequence?): Filter.FilterResults? {

            val results = Filter.FilterResults()

            if (prefix == null || prefix.isEmpty()) {
                var list: ArrayList<Station> = ArrayList()
                synchronized(mLock) {
                    list = ArrayList(mOriginalValues)
                }
                results.values = list
                results.count = list.size
            } else {
                val prefixString = prefix.toString().toLowerCase()

                var values: ArrayList<Station> = ArrayList()
                synchronized(mLock) {
                    values = ArrayList(mOriginalValues)
                }

                val count = values.size
                val newValues = ArrayList<Station>()

                for (i in 0 until count) {
                    val value = values[i]
                    val valueText = value.nameStation!!.toLowerCase()

                    if (valueText.startsWith(prefixString)) {
                        newValues.add(value)
                    } else {
                        val words = valueText.split("\\s+".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
                        for (word in words) {
                            if (word.startsWith(prefixString)) {
                                newValues.add(value)
                                break
                            }
                        }
                    }
                }

                results.values = newValues
                results.count = newValues.size
            }
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: Filter.FilterResults) {
            mObjects.clear()
            mObjects.addAll(results.values as MutableList<Station>)
            dispatchMarkers()
            if (results.count > 0) {
                notifyDataSetChanged()
            } else {
                notifyDataSetInvalidated()
            }
        }


    }

    private fun dispatchMarkers() {
        if (mObjects.isEmpty()) {
            callback.setMarkers(mOriginalValues)
        } else {
            callback.setMarkers(mObjects)
        }
    }

    interface Callback {
        fun setMarkers(list: MutableList<Station>)
        fun onStationClicked(id: Long)
    }

    fun getIdByLatLng(position: LatLng): Long {
            for (item in mOriginalValues) {
                if (item.lat == position.latitude && item.lng == position.longitude) {
                    return item.id
                }
            }
        return -1
    }

    fun getItemByLatLng(position: LatLng): Station? {
        for (item in mOriginalValues) {
            if (item.lat == position.latitude && item.lng == position.longitude) {
                return item
            }
        }
        return null
    }
}