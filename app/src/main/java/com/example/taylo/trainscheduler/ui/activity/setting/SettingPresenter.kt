package com.example.taylo.trainscheduler.ui.activity.setting

import com.example.taylo.trainscheduler.data.DataManager
import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.ui.base.BasePresenter
import com.example.taylo.trainscheduler.utils.rx.ISchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject


class SettingPresenter
@Inject
constructor(schedulerProvider: ISchedulerProvider,
            dataManager: DataManager,
            compositeDisposable: CompositeDisposable) : BasePresenter<ISettingView>(schedulerProvider , dataManager, compositeDisposable) , ISettingPresenter {

    override fun refresh() {
        loadData()
    }

    override fun onAttach(view: ISettingView) {
        super.onAttach(view)
        loadData()
    }

    private fun loadData() {
        mvpView?.onProvideResource(Resource.loading(null, dataManager.isSyncing()))

        compositeDisposable.add(dataManager.getAllTimeTableSingle("", false)
                .subscribe({ resource ->
                    Timber.d("loadData success")
                    mvpView?.onProvideResource(resource)
                }, {t ->
                    Timber.e(t, "loadData error")
                }))
    }

    fun sync() {
        dataManager.triggerSync(true)
    }
}