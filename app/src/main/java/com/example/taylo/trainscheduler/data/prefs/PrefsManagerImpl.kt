package com.example.taylo.trainscheduler.data.prefs

import android.annotation.SuppressLint
import android.content.SharedPreferences
import timber.log.Timber

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@SuppressLint("ApplySharedPref")
class PrefsManagerImpl @Inject
constructor(private val preferences: SharedPreferences) : PrefsManager {

    companion object {
        private const val KEY_LAST_UPDATED_TIME = "KEY_LAST_UPDATED_TIME2"
        private const val KEY_LAST_UPDATED_TIME_WITH_FIREBASE = "KEY_LAST_UPDATED_TIME_WITH_FIREBASE2"
        private const val KEY_IS_FIRST_LAUNCH = "KEY_IS_FIRST_LAUNCH2"
    }

    override fun getLastUpdatedTime(): Long {
        return preferences.getLong(KEY_LAST_UPDATED_TIME, 0)
    }

    override fun saveLastUpdatedTime(time: Long) {
        preferences.edit().putLong(KEY_LAST_UPDATED_TIME, time).commit()
    }

    override fun isFirstLaunch(): Boolean {
        return preferences.getBoolean(KEY_IS_FIRST_LAUNCH, true)
    }

    override fun saveFirstLaunch(isFirstLaunch: Boolean) {
        Timber.d("saveFirstLaunch %s", isFirstLaunch)
        preferences.edit().putBoolean(KEY_IS_FIRST_LAUNCH, isFirstLaunch).commit()
    }

    override fun saveLastUpdatedTimeWithFirebase(time: Long) {
        preferences.edit().putLong(KEY_LAST_UPDATED_TIME_WITH_FIREBASE, time).commit()
    }

    override fun getLastUpdatedTimeWithFirebase(): Long {
        return preferences.getLong(KEY_LAST_UPDATED_TIME_WITH_FIREBASE, 0)
    }
}
