package com.example.taylo.trainscheduler.data

import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.station.Station
import io.reactivex.Single

interface DataManager {

    fun isSyncing(): Boolean

    fun triggerSync(isForce: Boolean = false)

    fun getAllTimeTableSingle(filterParam: String, withDetail: Boolean): Single<Resource<MutableList<Station>>>

    fun getFavoritesTimeTableSingle(withDetail: Boolean): Single<Resource<MutableList<Station>>>

    fun updateStation(item: Station)

    fun getDetailStationSingle(id: Long): Single<Resource<Station>>

    fun updateFavorites(stations: MutableList<Station>)
}