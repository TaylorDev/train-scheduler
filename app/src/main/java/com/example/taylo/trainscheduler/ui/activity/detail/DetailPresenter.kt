package com.example.taylo.trainscheduler.ui.activity.detail

import com.example.taylo.trainscheduler.data.DataManager
import com.example.taylo.trainscheduler.di.ActivityScope
import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.base.BasePresenter
import com.example.taylo.trainscheduler.utils.rx.ISchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject

@ActivityScope
class DetailPresenter
@Inject
constructor(schedulerProvider: ISchedulerProvider,
            dataManager: DataManager,
            compositeDisposable: CompositeDisposable): BasePresenter<DetailMvpView>(schedulerProvider, dataManager, compositeDisposable), IDetailPresenter {

    override fun setFavorite(station: Station) {
        dataManager.updateStation(station)
    }

    private var stationId: Long = 0

    private fun loadDetail() {

        mvpView?.onProvideResource(Resource.loading(null, dataManager.isSyncing()))

        dataManager.getDetailStationSingle(stationId)
                .subscribe({ resource->
                    mvpView?.onProvideResource(resource)
                }, { t ->
                    Timber.e(t)
                    mvpView?.onProvideResource(Resource.error("Ошибка", null, dataManager.isSyncing()))
                })
    }

    override fun refresh() {
        loadDetail()
    }

    override fun setStationId(id: Long) {
        stationId = id
    }
}