package com.example.taylo.trainscheduler.model.station

import com.google.gson.annotations.SerializedName

data class StationResponse(

	@SerializedName("station")
	val station: MutableList<Station> = mutableListOf()
)