package com.example.taylo.trainscheduler.ui.activity.main

import com.example.taylo.trainscheduler.ui.base.IBaseView

interface IMainView : IBaseView {
    fun showSyncing(syncing: Boolean)
}