package com.example.taylo.trainscheduler.ui.activity.main

import com.example.taylo.trainscheduler.di.ActivityScope
import com.example.taylo.trainscheduler.ui.base.IBasePresenter

@ActivityScope
interface IMainPresenter: IBasePresenter<IMainView> {
}