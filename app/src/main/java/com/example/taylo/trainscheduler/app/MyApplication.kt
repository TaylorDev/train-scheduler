package com.example.taylo.trainscheduler.app

import android.app.Application
import android.os.Handler
import com.example.taylo.trainscheduler.BuildConfig
import com.example.taylo.trainscheduler.data.DataManager
import com.example.taylo.trainscheduler.data.DataManagerImpl
import com.example.taylo.trainscheduler.di.component.ApplicationComponent
import com.example.taylo.trainscheduler.di.component.DaggerApplicationComponent
import com.example.taylo.trainscheduler.di.module.ApplicationModule
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.model.station.StationScheduleTest
import com.example.taylo.trainscheduler.model.station.StationTest
import com.facebook.stetho.Stetho
import com.google.firebase.FirebaseApp
import com.google.firebase.firestore.FirebaseFirestore
import io.objectbox.BoxStore
import io.objectbox.android.AndroidObjectBrowser
import io.reactivex.Observable
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase




class MyApplication : Application() {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var boxStore: BoxStore

    companion object {
        lateinit var appComponent : ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this)).build()

        appComponent.inject(this)

        if (BuildConfig.DEBUG) {
//            AndroidObjectBrowser(boxStore).start(this)
        }

        Timber.plant(Timber.DebugTree())
        Stetho.initializeWithDefaults(this)

        Timber.d("MyApplication onCreate")

        FirebaseApp.initializeApp(this)

        dataManager.triggerSync()

        /*val firebaseApp = FirebaseApp.initializeApp(this)
        dataManager.getAllTimeTableSingle("", true)
                .toObservable()
                .flatMap { Observable.fromIterable(it.data) }
                .map {
                    uploadItemOnFirebase(it)
                    it
                }
                .subscribe({
                }, {
                    Timber.e(it)
                })*/

    }

    private fun uploadItemOnFirebase(item: Station) {
        try {
            val db = FirebaseFirestore.getInstance()
//            val database = FirebaseDatabase.getInstance()
//            val myRef = database.getReference("example")


            val testItem = StationTest()
            testItem.lat = item.lat
            testItem.lng = item.lng
            testItem.nameStation = item.nameStation + "_TEST"
            for (scheduleItem in item.scheduleList) {
                val testScheduleItem = StationScheduleTest()
                testScheduleItem.isEveryDay = scheduleItem.isEveryDay
                testScheduleItem.isWeekend = scheduleItem.isWeekend
                testScheduleItem.isWeekdays = scheduleItem.isWeekdays
                testScheduleItem.onlyDates = scheduleItem.onlyDates
                testScheduleItem.exceptDates = scheduleItem.exceptDates
                testScheduleItem.fromDates = scheduleItem.fromDates
                testScheduleItem.byDates = scheduleItem.byDates
                testScheduleItem.time = scheduleItem.time
                testItem.scheduleList.add(testScheduleItem)
            }
            Timber.d("send %s", testItem.nameStation)
            db.collection(DataManagerImpl.FIREBASE_TEST_TABLE).add(testItem)
                    .addOnCompleteListener {
                        Timber.d("on complete %s", it.isSuccessful)
                    }
//            myRef.setValue(testItem)
        } catch (throwable: Throwable) {
            Timber.e(throwable, "loadTestListOnFirebase error")
        }
    }

    private fun loadTestListOnFirebase(list: MutableList<Station>) {
        Timber.d("loadTestListOnFirebase")
        try {
            val db = FirebaseFirestore.getInstance()
            for (item in list) {
                val testItem = StationTest()
                testItem.lat = item.lat
                testItem.lng = item.lng
                testItem.nameStation = item.nameStation + "_TEST"
                for (scheduleItem in item.scheduleList) {
                    val testScheduleItem = StationScheduleTest()
                    testScheduleItem.isEveryDay = scheduleItem.isEveryDay
                    testScheduleItem.isWeekend = scheduleItem.isWeekend
                    testScheduleItem.isWeekdays = scheduleItem.isWeekdays
                    testScheduleItem.onlyDates = scheduleItem.onlyDates
                    testScheduleItem.exceptDates = scheduleItem.exceptDates
                    testScheduleItem.fromDates = scheduleItem.fromDates
                    testScheduleItem.byDates = scheduleItem.byDates
                    testScheduleItem.time = scheduleItem.time
                    testItem.scheduleList.add(testScheduleItem)
                }
                Timber.d("send")
                db.collection(DataManagerImpl.FIREBASE_TEST_TABLE).add(testItem)
            }
        } catch (throwable: Throwable) {
            Timber.e(throwable, "loadTestListOnFirebase error")
        }
        Timber.d("loadTestListOnFirebase complete")
    }

    fun getComponent() : ApplicationComponent {
        return appComponent
    }
}