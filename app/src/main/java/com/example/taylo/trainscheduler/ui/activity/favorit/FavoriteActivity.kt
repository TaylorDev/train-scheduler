package com.example.taylo.trainscheduler.ui.activity.favorit

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.example.taylo.trainscheduler.R
import com.example.taylo.trainscheduler.adapter.FavoriteDelegateAdapter
import com.example.taylo.trainscheduler.adapter.base.CompositeDelegateAdapter
import com.example.taylo.trainscheduler.event_bus.SyncEvent
import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.Status
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.activity.add_favorit.AddFavoriteActivity
import com.example.taylo.trainscheduler.ui.activity.detail.DetailActivity
import com.example.taylo.trainscheduler.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_favorit.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

class FavoriteActivity : BaseActivity(), IFavoriteView, FavoriteDelegateAdapter.Callback {

    @Inject
    lateinit var presenter : FavoritePresenterImpl

    companion object {

        const val RC_ADD_FAVORITE = 1
        const val RC_DETAIL = 2

        fun start(context: Context) {
            val intent = Intent(context, FavoriteActivity::class.java)
            context.startActivity(intent)
        }
    }

    lateinit var adapter: CompositeDelegateAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorit)
        getActivityComponent().inject(this)
        title = "ИЗБРАННОЕ"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        addFavoriteBtn.setOnClickListener({
            AddFavoriteActivity.startForResult(this, RC_ADD_FAVORITE)
        })

        adapter = CompositeDelegateAdapter.Builder()
                .add(FavoriteDelegateAdapter(this))
                .build()

        list.layoutManager = LinearLayoutManager(this)
        list.adapter = adapter

        swipeRefresh.setOnRefreshListener { presenter.refresh() }

        presenter.onAttach(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.refresh()
    }

    override fun onItemLongClick(station: Station, position: Int) {
        presenter.removeFromFavorites(station)
        adapter.removeItem(position)
    }

    override fun onItemClick(station: Station, position: Int) {
        DetailActivity.startForResult(this, station.id, RC_DETAIL)
    }

    override fun onProvideResource(resource: Resource<MutableList<Station>>) {
        adapter.swapData(resource.data)
        swipeRefresh.isRefreshing = resource.status == Status.LOADING

        if (resource.isSyncing) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    override fun onSyncStateChanged(event: SyncEvent) {
        if (event.isSyncing) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
            presenter.refresh()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
