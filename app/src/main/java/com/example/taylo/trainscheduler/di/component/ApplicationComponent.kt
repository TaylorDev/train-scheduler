package com.example.taylo.trainscheduler.di.component

import android.content.Context
import com.example.taylo.trainscheduler.app.MyApplication
import com.example.taylo.trainscheduler.data.DataManager
import com.example.taylo.trainscheduler.di.module.ApplicationModule
import com.example.taylo.trainscheduler.notifications.AlarmReceiver
import com.example.taylo.trainscheduler.utils.rx.ISchedulerProvider
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun inject(app: MyApplication)

    fun inject(app: AlarmReceiver)

    fun context(): Context

    fun dataManager(): DataManager

    fun application(): MyApplication

    fun schedulerProvider(): ISchedulerProvider

}