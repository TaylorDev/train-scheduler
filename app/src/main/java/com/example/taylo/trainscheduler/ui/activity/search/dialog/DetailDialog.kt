package com.example.taylo.trainscheduler.ui.activity.search.dialog

import android.graphics.Point
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.taylo.trainscheduler.R
import com.example.taylo.trainscheduler.app.MyApplication
import com.example.taylo.trainscheduler.di.component.ActivityComponent
import com.example.taylo.trainscheduler.di.component.DaggerActivityComponent
import com.example.taylo.trainscheduler.di.module.ActivityModule
import com.example.taylo.trainscheduler.event_bus.SyncEvent
import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.activity.detail.DetailMvpView
import com.example.taylo.trainscheduler.ui.activity.detail.DetailPresenter
import kotlinx.android.synthetic.main.marker_info_window.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

class DetailDialog : DialogFragment(), DetailMvpView {

    private lateinit var activityComponent : ActivityComponent

    @Inject
    lateinit var presenter: DetailPresenter

    companion object {
        private const val ARG_STATION_ID = "ARG_STATION_ID"
        private const val TAG = "DetailDialog"

        fun show(activity: FragmentActivity, id: Long): DetailDialog {
            val fragment = DetailDialog()
            val args = Bundle()
            args.putLong(ARG_STATION_ID, id)
            fragment.arguments = args
            fragment.show(activity.supportFragmentManager, DetailDialog.TAG)
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
        val window = dialog.window
        val display = window!!.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        window.setLayout((width * 0.9f).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent = DaggerActivityComponent.builder()
                .activityModule(ActivityModule(activity as AppCompatActivity))
                .applicationComponent((MyApplication.appComponent))
                .build()
        activityComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.marker_info_window, container, false)
        presenter.onAttach(this)
        presenter.setStationId(arguments.getLong(ARG_STATION_ID))
        presenter.refresh()
        EventBus.getDefault().register(this)
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        EventBus.getDefault().unregister(this)
        presenter.onDetach()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onSyncStateChanged(event: SyncEvent) {
        presenter.refresh()
    }

    override fun onProvideResource(resource: Resource<Station>) {
        if (resource.data != null) {
            val stationItem = resource.data

            val mytitle = "ст. " + stationItem.nameStation
            nameTextView.text = mytitle

            setFavorite(stationItem.isFavorite)

            /*favoriteImageView.setOnClickListener {
                stationItem.isFavorite = !stationItem.isFavorite
                presenter.setFavorite(stationItem)
                setFavorite(stationItem.isFavorite)
            }*/

            when(stationItem.findState()) {
                Station.STATE.OPEN -> {
                    isClosedTextView.text = "Сейчас открыта"
                    imageView.setColorFilter(ContextCompat.getColor(context, R.color.green))
                    closeTimeTextView.text = "Закроется через " + stationItem.getNextTime(true)
                }
                Station.STATE.CLOSED -> {
                    isClosedTextView.text = "Сейчас закрыта"
                    imageView.setColorFilter(ContextCompat.getColor(context, R.color.red))
                    closeTimeTextView.text = "Откроется через " + stationItem.getNextTime(false)
                }
                Station.STATE.UNKNOWN -> {
                    isClosedTextView.text = "Нет информации"
                    imageView.setColorFilter(ContextCompat.getColor(context, R.color.black))
                    closeTimeTextView.text = "Нет информации"
                }
            }
        }
    }

    private fun setFavorite(isFavorite: Boolean) {
        /*if (isFavorite) {
            favoriteImageView.setColorFilter(ContextCompat.getColor(context, R.color.orange))
        } else {
            favoriteImageView.setColorFilter(ContextCompat.getColor(context, R.color.grey))
        }*/
    }







    override fun showLoading(message: String?) {

    }

    override fun showLoading() {

    }

    override fun hideLoading() {

    }

    override fun onError(errorString: String) {

    }

    override fun onError(resId: Int) {

    }

    override fun networkConnected(): Boolean {
        return true
    }

}
