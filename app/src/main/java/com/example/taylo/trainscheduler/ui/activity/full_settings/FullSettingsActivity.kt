package com.example.taylo.trainscheduler.ui.activity.full_settings

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.example.taylo.trainscheduler.R
import com.example.taylo.trainscheduler.event_bus.SyncEvent
import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.base.BaseActivity
import com.example.taylo.trainscheduler.utils.TimeUtils
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject
import kotlinx.android.synthetic.main.activity_full_settings.*

class FullSettingsActivity : BaseActivity(), IFullSettingsView {

    @Inject
    lateinit var presenter : FullSettingsPresenter

    companion object {

        private const val EXTRA_STATION_ID = "EXTRA_STATION_ID"

        fun start(context: Context, stationId: Long) {
            val intent = Intent(context, FullSettingsActivity::class.java)
            intent.putExtra(EXTRA_STATION_ID, stationId)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_settings)
        getActivityComponent().inject(this)

        title = "НАСТРОЙКИ"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        presenter.setStationId(intent.getLongExtra(EXTRA_STATION_ID, 0))
        presenter.onAttach(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("SetTextI18n")
    override fun onProvideResource(resource: Resource<Station>) {
        if (resource.data != null) {
            val stationItem = resource.data
            val title = "Ст. " + resource.data.nameStation
            titleTextView.text = title
            checkBox.isChecked = resource.data.hasNotification
            if (stationItem.isAlwaysNotify) {
                radioGroup.check(R.id.alwaysRadioButton)
            } else {
                radioGroup.check(R.id.intervalRadioButton)
            }
            if (stationItem.beforeNotificationTimeMinutes == 0) {
                beforeMinEditText.setText("")
            } else {
                beforeMinEditText.setText(resource.data.beforeNotificationTimeMinutes.toString())
            }
            beforeMinEditText.setSelection(beforeMinEditText.text.length)
            fromTimeTextView.text = TimeUtils.getFormattedStr(stationItem.notifyFromHour) + ":" + TimeUtils.getFormattedStr(stationItem.notifyFromMinute)
            untilTimeTextView.text =  TimeUtils.getFormattedStr(stationItem.notifyUntilHour) + ":" + TimeUtils.getFormattedStr(stationItem.notifyUntilMinute)
            fromTimeTextView.setOnClickListener {
                val timePickerDialog = TimePickerDialog.newInstance({ _, hourOfDay, minute, _ ->
                    stationItem.notifyFromHour = hourOfDay
                    stationItem.notifyFromMinute = minute
                    fromTimeTextView.text = TimeUtils.getFormattedStr(hourOfDay) + ":" + TimeUtils.getFormattedStr(minute)
                }, stationItem.notifyFromHour, stationItem.notifyFromMinute, true)
                timePickerDialog.show(fragmentManager, "TimePickerDialog1")
            }
            untilTimeTextView.setOnClickListener {
                val timePickerDialog = TimePickerDialog.newInstance({ _, hourOfDay, minute, _ ->
                    stationItem.notifyUntilHour = hourOfDay
                    stationItem.notifyUntilMinute = minute
                    untilTimeTextView.text = TimeUtils.getFormattedStr(hourOfDay) + ":" + TimeUtils.getFormattedStr(minute)
                }, stationItem.notifyUntilHour, stationItem.notifyUntilMinute, true)
                timePickerDialog.show(fragmentManager, "TimePickerDialog2")
            }

            saveBtn.setOnClickListener({
                if (stationItem.scheduleList.size == 0) {
                    Toast.makeText(this, "Данные синхронизируются", Toast.LENGTH_SHORT).show()
                } else {
                    val minStr = beforeMinEditText.text.toString()
                    stationItem.beforeNotificationTimeMinutes = if (minStr.isEmpty()) {
                        0
                    } else {
                        minStr.toInt()
                    }
                    if (presenter.onSaveClicked(stationItem)) {
                        finish()
                    } else {
                        Toast.makeText(this, "Невозможно установить уведомление", Toast.LENGTH_SHORT).show()
                    }
                }
            })
            checkBox.setOnCheckedChangeListener({ _, isChecked ->
                stationItem.hasNotification = isChecked
            })
            radioGroup.setOnCheckedChangeListener { _, checkedId ->
                when(checkedId) {
                    R.id.alwaysRadioButton -> stationItem.isAlwaysNotify = true
                    R.id.intervalRadioButton -> stationItem.isAlwaysNotify = false
                }
            }
        }
        if (resource.isSyncing) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    override fun onSyncStateChanged(event: SyncEvent) {
        if (event.isSyncing) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
            presenter.refresh()
        }
    }
}
