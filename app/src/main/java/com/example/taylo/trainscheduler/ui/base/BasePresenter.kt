package com.example.taylo.trainscheduler.ui.base

import com.example.taylo.trainscheduler.data.DataManager
import com.example.taylo.trainscheduler.utils.rx.ISchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

open class BasePresenter<V : IBaseView>
@Inject
constructor(val schedulerProvider: ISchedulerProvider,
            val dataManager: DataManager,
            val compositeDisposable: CompositeDisposable) : IBasePresenter<V> {


    var mvpView: V? = null
        private set

    override fun onAttach(view: V) {
        mvpView = view
    }

    override fun onDetach() {
        mvpView = null
        compositeDisposable.dispose()
    }
    companion object {
        private val TAG = "BasePresenter"
    }
}