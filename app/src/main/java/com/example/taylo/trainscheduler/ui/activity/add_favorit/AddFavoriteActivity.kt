package com.example.taylo.trainscheduler.ui.activity.add_favorit

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.example.taylo.trainscheduler.R
import com.example.taylo.trainscheduler.adapter.AddFavoriteDelegateAdapter
import com.example.taylo.trainscheduler.adapter.base.CompositeDelegateAdapter
import com.example.taylo.trainscheduler.event_bus.SyncEvent
import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.Status
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_add_favorit.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

class AddFavoriteActivity : BaseActivity(), IAddFavoriteView, SearchView.OnQueryTextListener {

    @Inject
    lateinit var presenter : AddFavoritePresenter

    private lateinit var searchView: SearchView

    companion object {
        fun startForResult(activity: Activity, requestCode: Int) {
            activity.startActivityForResult(Intent(activity, AddFavoriteActivity::class.java), requestCode)
        }
    }

    lateinit var adapter: CompositeDelegateAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_favorit)
        getActivityComponent().inject(this)

        title = "ДОБАВИТЬ В ИЗБРАННОЕ"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        adapter = CompositeDelegateAdapter.Builder()
                .add(AddFavoriteDelegateAdapter())
                .build()

        list_item_fav.layoutManager = LinearLayoutManager(this)
        list_item_fav.adapter = adapter

        swipeRefresh.setOnRefreshListener { presenter.refresh() }

        presenter.onAttach(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun onProvideResource(resource: Resource<MutableList<Station>>) {
        swipeRefresh.isRefreshing = resource.status == Status.LOADING

        if (resource.data != null) {
            adapter.swapData(resource.data)
            saveBtn.setOnClickListener {
                presenter.save(resource.data)
                finish()
            }
        }

        if (resource.isSyncing) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    override fun onSyncStateChanged(event: SyncEvent) {
        if (event.isSyncing) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
            presenter.refresh()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)

        val searchItem = menu.findItem(R.id.action_search)
        searchView = MenuItemCompat.getActionView(searchItem) as SearchView
        searchView.setOnQueryTextListener(this)

        return true
    }

   override fun onQueryTextChange(query: String): Boolean {
       presenter.search(query)
       return false
    }

   override fun onQueryTextSubmit(query: String): Boolean {
        return false
   }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
