package com.example.taylo.trainscheduler.adapter.base

import android.view.LayoutInflater
import android.view.ViewGroup

abstract class BaseDelegateAdapter<in T> : IDelegateAdapter<T> {

    override final fun createViewHolder(parent: ViewGroup, viewType: Int): KViewHolder {
        val inflatedView = LayoutInflater.from(parent.context).inflate(getLayoutId(), parent, false)
        return KViewHolder(inflatedView)
    }

    @Suppress("UNCHECKED_CAST")
    override final fun bindViewHolder(holder: KViewHolder, item: Any, position: Int) {
        onBindViewHolder(holder, item as T, position)
    }

    override fun recycled(holder: KViewHolder) {

    }
}