package com.example.taylo.trainscheduler.ui.activity.search

import com.example.taylo.trainscheduler.data.DataManager
import com.example.taylo.trainscheduler.di.ActivityScope
import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.ui.base.BasePresenter
import com.example.taylo.trainscheduler.utils.rx.ISchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject

@ActivityScope
class SearchPresenter
@Inject
constructor(schedulerProvider: ISchedulerProvider,
            dataManager: DataManager,
            compositeDisposable: CompositeDisposable): BasePresenter<SearchMvpView>(schedulerProvider, dataManager, compositeDisposable), ISearchPresenter {

    override fun onAttach(view: SearchMvpView) {
        super.onAttach(view)
        loadTimeTable()
    }

    fun loadTimeTable() {

        mvpView?.onProvideResource(Resource.loading(null, dataManager.isSyncing()))

        compositeDisposable.add(dataManager.getAllTimeTableSingle("", true)
                .subscribe({ resource ->
                    Timber.d("loadTimeTable success")
                    mvpView?.onProvideResource(resource)
                }, { t ->
                    Timber.e(t, "my error")
                }))
    }


    override fun refresh() {
        loadTimeTable()
    }
}
