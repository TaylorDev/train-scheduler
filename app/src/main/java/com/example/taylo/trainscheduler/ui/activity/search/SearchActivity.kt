package com.example.taylo.trainscheduler.ui.activity.search

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.taylo.trainscheduler.R
import com.example.taylo.trainscheduler.event_bus.SyncEvent
import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.activity.detail.DetailActivity
import com.example.taylo.trainscheduler.ui.base.BaseActivity
import com.example.taylo.trainscheduler.utils.AndroidUtils
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_search.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import timber.log.Timber
import javax.inject.Inject
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.CameraUpdateFactory

class SearchActivity : BaseActivity(), OnMapReadyCallback, SearchMvpView, MyAdapter.Callback {

    @Inject
    lateinit var presenter: SearchPresenter

    private lateinit var adapter: MyAdapter

    private var googleMap: GoogleMap? = null

    companion object {

        private const val RC_DETAIL = 1

        private val RC_LOCATION_PERMISSION = 100

        fun start(context: Context) {
            context.startActivity(Intent(context, SearchActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        getActivityComponent().inject(this)
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "ПОИСК ЖД ПЕРЕЕЗДА"

        adapter = MyAdapter(this, this, R.layout.support_simple_spinner_dropdown_item, mutableListOf())
        autocompleteTextView.setAdapter(adapter)

        presenter.onAttach(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        googleMap.setOnInfoWindowClickListener {
            Timber.d("title click %s", it.position)
            onStationClicked(adapter.getIdByLatLng(it.position))
        }
        googleMap.setPadding(0, AndroidUtils.dpToPx(60f), 0, 0)
        /*googleMap.setOnMarkerClickListener {
            DetailDialog.show(this, adapter.getIdByLatLng(it.position))
            true
        }*/
        googleMap.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
            override fun getInfoWindow(marker: Marker): View? {
                return null
            }

            override fun getInfoContents(marker: Marker): View? {
                val view = layoutInflater.inflate(R.layout.marker_info_window, null) as View
                val nameTextView = view.findViewById(R.id.nameTextView) as TextView
                val imageView = view.findViewById(R.id.imageView) as ImageView
                val isClosedTextView = view.findViewById(R.id.isClosedTextView) as TextView
                val closeTimeTextView = view.findViewById(R.id.closeTimeTextView) as TextView

                val stationItem = adapter.getItemByLatLng(marker.position)
                if (stationItem != null) {
                    val mytitle = "ст. " + stationItem.nameStation
                    nameTextView.text = mytitle

                    when(stationItem.findState()) {
                        Station.STATE.OPEN -> {
                            isClosedTextView.text = "Сейчас открыта"
                            imageView.setColorFilter(ContextCompat.getColor(this@SearchActivity, R.color.green))
                            closeTimeTextView.text = "Закроется через " + stationItem.getNextTime(true)
                        }
                        Station.STATE.CLOSED -> {
                            isClosedTextView.text = "Сейчас закрыта"
                            imageView.setColorFilter(ContextCompat.getColor(this@SearchActivity, R.color.red))
                            closeTimeTextView.text = "Откроется через " + stationItem.getNextTime(false)
                        }
                        Station.STATE.UNKNOWN -> {
                            isClosedTextView.text = "Нет информации"
                            imageView.setColorFilter(ContextCompat.getColor(this@SearchActivity, R.color.black))
                            closeTimeTextView.text = "Нет информации"
                        }
                    }
                }

                return view
            }
        })
        if (checkLocationPermissions()) {
            googleMap.isMyLocationEnabled = true
        } else {
            requestLocationPermissions()
        }
    }

    private fun onPermissionLocationGranted() {
        googleMap?.isMyLocationEnabled = true
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    override fun onSyncStateChanged(event: SyncEvent) {
        if (event.isSyncing) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
            presenter.refresh()
        }
    }

    override fun onProvideResource(resource: Resource<MutableList<Station>>) {
        if (resource.data != null) {
            adapter.setData(resource.data)
        }

        if (resource.isSyncing) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
        }
    }

    override fun setMarkers(list: MutableList<Station>) {
        Timber.d("setMarkers")
        if (googleMap != null) {
            googleMap!!.clear()

            if (list.isNotEmpty()) {

                if (list.size == 1) {
                    val latLng = LatLng(list[0].lat!!, list[0].lng!!)
                    googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f))
                    setMarker(list[0])
                } else {
                    val builder = LatLngBounds.Builder()
                    for (item in list) {
                        val latLng = LatLng(item.lat!!, item.lng!!)
                        setMarker(item)
                        builder.include(latLng)
                    }

                    val bounds = builder.build()

                    val cu = CameraUpdateFactory.newLatLngBounds(bounds, 200)
                    googleMap!!.animateCamera(cu)
                }
            }
        }
    }

    override fun onStationClicked(id: Long) {
        DetailActivity.startForResult(this, id, RC_DETAIL)
    }

    private fun setMarker(station: Station): Marker {
        val marker = googleMap!!.addMarker(MarkerOptions().position(LatLng(station.lat!!, station.lng!!)))
        marker.title = station.nameStation
        return marker
    }

    override fun onStart() {
        super.onStart()
        if (mapView != null) {
            mapView.onStart()
        }
    }

    override fun onResume() {
        super.onResume()
        if (mapView != null) {
            mapView.onResume()
        }
    }

    override fun onPause() {
        super.onPause()
        if (mapView != null) {
            mapView.onPause()
        }
    }

    override fun onStop() {
        super.onStop()
        if (mapView != null) {
            mapView.onStop()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (mapView != null) {
            mapView.onSaveInstanceState(outState)
        }
    }

    override fun onDestroy() {
        if (mapView != null) {
            mapView.onDestroy()
        }
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        if (mapView != null) {
            mapView.onLowMemory()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            RC_LOCATION_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults.size > 1 && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    onPermissionLocationGranted()
                }
                return
            }
        }
    }



    fun requestLocationPermissions() {
        ActivityCompat.requestPermissions(this, arrayOf(ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION), RC_LOCATION_PERMISSION)
    }

    fun checkLocationPermissions(): Boolean {
        return ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }
}