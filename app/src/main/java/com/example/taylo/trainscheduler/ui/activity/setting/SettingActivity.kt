package com.example.taylo.trainscheduler.ui.activity.setting

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.example.taylo.trainscheduler.R
import com.example.taylo.trainscheduler.adapter.SettingsDelegateAdapter
import com.example.taylo.trainscheduler.adapter.base.CompositeDelegateAdapter
import com.example.taylo.trainscheduler.event_bus.SyncEvent
import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.activity.full_settings.FullSettingsActivity
import com.example.taylo.trainscheduler.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_setting.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

class SettingActivity : BaseActivity(), ISettingView, SettingsDelegateAdapter.Callback {

    @Inject
    lateinit var presenter : SettingPresenter

    lateinit var adapter: CompositeDelegateAdapter

    companion object {

        fun getStartIntent(context: Context): Intent {
            return Intent(context, SettingActivity::class.java)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        getActivityComponent().inject(this)
        title = "НАСТРОЙКИ"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        adapter = CompositeDelegateAdapter.Builder()
                .add(SettingsDelegateAdapter(this))
                .build()

        val layoutManager = LinearLayoutManager(this)
        val dividerItemDecoration = DividerItemDecoration(recyclerView.context, layoutManager.orientation)
        recyclerView.addItemDecoration(dividerItemDecoration)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter

        presenter.onAttach(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun onItemClicked(item: Station) {
        FullSettingsActivity.start(this, item.id)
    }

    override fun onProvideResource(resource: Resource<MutableList<Station>>) {
        adapter.swapData(resource.data)

        if (resource.isSyncing) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    override fun onSyncStateChanged(event: SyncEvent) {
        if (event.isSyncing) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
            presenter.refresh()
        }
    }

    /*override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_settings, menu)
        return true
    }*/

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            /*R.id.action_sync -> {
                presenter.sync()
                return true
            }*/
        }
        return super.onOptionsItemSelected(item)
    }
}
