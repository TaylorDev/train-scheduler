package com.example.taylo.trainscheduler.model.station

import java.util.ArrayList

data class StationTest(

        var nameStation: String? = null,

        var lat: Double? = null,

        var lng: Double? = null,

        var scheduleList: MutableList<StationScheduleTest> = ArrayList()
)
