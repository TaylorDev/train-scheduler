package com.example.taylo.trainscheduler.adapter.base

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.extensions.LayoutContainer

open class KViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer