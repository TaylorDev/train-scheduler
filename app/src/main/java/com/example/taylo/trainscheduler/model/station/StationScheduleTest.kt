package com.example.taylo.trainscheduler.model.station

import java.util.ArrayList

data class StationScheduleTest(
        var isEveryDay: Boolean = false,
        var isWeekend: Boolean = false,
        var isWeekdays: Boolean = false,
        var onlyDates: MutableList<String> = ArrayList(),
        var exceptDates: MutableList<String> = ArrayList(),
        var fromDates: MutableList<String> = ArrayList(),
        var byDates: MutableList<String> = ArrayList(),
        var time: String = ""
)
