package com.example.taylo.trainscheduler.ui.activity.about

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.taylo.trainscheduler.R

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
    }
}
