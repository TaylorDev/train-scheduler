package com.example.taylo.trainscheduler.utils

import java.util.*

object TimeUtils {

    fun getStringTime(milliseconds: Long): String {
        return getStringTime(milliseconds, false, false)
    }

    fun getStringTime(milliseconds: Long, withChars: Boolean): String {
        return getStringTime(milliseconds, false, withChars)
    }

    fun getStringTime(milliseconds: Long, withHour: Boolean, withChars: Boolean): String {
        val second = milliseconds.toFloat() / 1000f % 60f
        val minute = milliseconds.toFloat() / (1000f * 60f) % 60f
        val hour = milliseconds.toFloat() / (1000f * 60f * 60f) % 24f

        val timeHour = getFormattedStr(hour.toInt())

        val timeMinute = getFormattedStr(minute.toInt())

        val timeString: String

        if (timeHour == "00" && !withHour) {
            timeString = if (withChars) {
                timeMinute + " мин"
            } else {
                timeMinute
            }
        } else {
            timeString = if (withChars) {
                "$timeHour ч $timeMinute мин"
            } else {
                "$timeHour:$timeMinute"
            }
        }
        return timeString
    }

    fun getFormattedStr(num: Int): String {
        return String.format(Locale.getDefault(), "%02d", num)
    }


    fun getCurrentTimeByMoscow(): Long {
        val calendar = getCalendarLocal()
        return calendar.timeInMillis
    }

    fun getCalendarLocal(): Calendar {
        return Calendar.getInstance(/*TimeZone.getTimeZone("Europe/Moscow")*/)
    }
}
