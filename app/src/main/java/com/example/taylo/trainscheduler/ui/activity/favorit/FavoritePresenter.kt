package com.example.taylo.trainscheduler.ui.activity.favorit

import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.base.IBasePresenter


interface FavoritePresenter: IBasePresenter<IFavoriteView> {
    fun refresh()
    fun removeFromFavorites(item: Station)
}