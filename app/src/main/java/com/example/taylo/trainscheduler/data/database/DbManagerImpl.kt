package com.example.taylo.trainscheduler.data.database

import com.example.taylo.trainscheduler.event_bus.SyncEvent
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.model.station.StationSchedule
import com.example.taylo.trainscheduler.model.station.Station_
import io.objectbox.BoxStore
import io.reactivex.Single
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DbManagerImpl
@Inject
constructor(private val boxStore: BoxStore) : DbManager {

    override fun updateTimeTable(list: MutableList<Station>) {
        val persistedList: MutableList<Station> =  boxStore.boxFor(Station::class.java).all
        for (persistedItem in persistedList.reversed()) {
            for (i in list.indices.reversed()) {
                val newItem = list[i]
                if (persistedItem.nameStation == newItem.nameStation) {
                    persistedItem.scheduleList = newItem.scheduleList
                    list.removeAt(i)
                    break
                }
            }
        }

        boxStore.boxFor(Station::class.java).removeAll()
        boxStore.boxFor(Station::class.java).put(persistedList)
        EventBus.getDefault().post(SyncEvent(false))
    }

    override fun updateTimeTableFromFirebase(newlist: MutableList<Station>) {
        val oldList: MutableList<Station> =  boxStore.boxFor(Station::class.java).all
        val listToSave = mutableListOf<Station>()
        for (i in oldList.indices.reversed()) {
            val oldItem = oldList[i]
            for (j in newlist.indices.reversed()) {
                val newItem = newlist[j]
                if (oldItem.nameStation == newItem.nameStation) {
                    addNewItemsToOldSchedule(oldItem.scheduleList, newItem.scheduleList)
                    newItem.lat?.let { oldItem.lat = it }
                    newItem.lng?.let { oldItem.lng = it }
                    newlist.removeAt(j)
                    listToSave.add(oldItem)
                    oldList.removeAt(i)
                    break
                }
            }
        }
        listToSave.addAll(newlist)

        for (i in oldList.indices.reversed()) {
            val item = oldList[i]
            if (item.url != null) {
                oldList.removeAt(i)
            }
        }

        boxStore.boxFor(Station::class.java).remove(oldList)
        boxStore.boxFor(Station::class.java).put(listToSave)
    }

    private fun addNewItemsToOldSchedule(oldList: MutableList<StationSchedule>, newList: MutableList<StationSchedule>) {
        for (i in newList.indices.reversed()) {
            val newItem = newList[i]
            for (oldItem in oldList) {
                if (newItem.time == oldItem.time) {
                    newList.removeAt(i)
                    break
                }
            }
        }
        oldList.addAll(newList)
    }

    override fun insertTimeTable(list: MutableList<Station>) {
        boxStore.boxFor(Station::class.java).removeAll()
        boxStore.boxFor(Station::class.java).put(list)
    }

    override fun findTimeTable(): Single<MutableList<Station>> {
        return Single.fromCallable {
            boxStore.boxFor(Station::class.java).all
        }
    }

    override fun findFavoriteTimeTable(): Single<MutableList<Station>> {
        return Single.fromCallable {
            boxStore.boxFor(Station::class.java).query().equal(Station_.isFavorite, true).build().find()
        }
    }

    override fun updateStation(item: Station) {
        boxStore.boxFor(Station::class.java).put(item)
    }

    override fun findStation(id: Long): Single<Station> {
        return Single.fromCallable {
            boxStore.boxFor(Station::class.java).query().equal(Station_.id, id).build().findFirst()
        }
    }

    override fun updateFavorites(stations: MutableList<Station>) {
        boxStore.boxFor(Station::class.java).put(stations)
    }
}
