package com.example.taylo.trainscheduler.di.module

import android.support.v7.app.AppCompatActivity
import com.example.taylo.trainscheduler.di.ActivityScope
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
class ActivityModule(private val activity: AppCompatActivity) {

    @Provides
    @ActivityScope
    fun provideContext(): AppCompatActivity {
        return activity
    }

    @Provides
    @ActivityScope
    fun provideCompositeDisposable(): CompositeDisposable {
        return CompositeDisposable()
    }
}