package com.example.taylo.trainscheduler.notifications

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import com.example.taylo.trainscheduler.model.station.Station
import java.util.*
import javax.inject.Inject

class ReminderManager
@Inject
constructor(private val context: Context) {

    fun setReminder(station: Station): Boolean {
        if (station.hasNotification) {
            val exactTime = computeExactTime(station)
            if (exactTime != null) {
                scheduleAlarm(station, exactTime)
            } else {
                return false
            }
        } else {
            cancelReminder(station)
        }
        return true
    }

    private fun computeExactTime(station: Station): Long? {
        return station.getNextCloseTimeForAlarm()
    }

    private fun scheduleAlarm(station: Station, exactTime: Long) {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = exactTime
        val pendingIntent = getPendingIntent(station)

        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, exactTime, pendingIntent)
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, exactTime, pendingIntent)
        }
    }

    private fun getPendingIntent(station: Station): PendingIntent {
        val intent = Intent(context, AlarmReceiver::class.java)
        intent.putExtra(AlarmReceiver.STATION_ITEM_ID, station.id)
        return PendingIntent.getBroadcast(context, station.id.toInt(), intent, PendingIntent.FLAG_CANCEL_CURRENT)
    }

    private fun cancelReminder(station: Station) {
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntent = getPendingIntent(station)
        alarmManager.cancel(pendingIntent)
    }
}
