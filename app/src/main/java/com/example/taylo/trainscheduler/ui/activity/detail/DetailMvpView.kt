package com.example.taylo.trainscheduler.ui.activity.detail

import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.base.IBaseView

interface DetailMvpView: IBaseView {
    fun onProvideResource(resource: Resource<Station>)
}