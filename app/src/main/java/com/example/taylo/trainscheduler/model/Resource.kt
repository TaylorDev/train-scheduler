package com.example.taylo.trainscheduler.model

import com.example.taylo.trainscheduler.model.Status.*

class Resource<out T>(val status: Status, val data: T?, val message: String?, val isSyncing: Boolean) {

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }

        val resource = other as Resource<*>?

        if (status !== resource!!.status) {
            return false
        }
        if (if (message != null) message != resource!!.message else resource!!.message != null) {
            return false
        }
        return if (data != null) data == resource.data else resource.data == null
    }

    override fun hashCode(): Int {
        var result = status.hashCode()
        result = 31 * result + (message?.hashCode() ?: 0)
        result = 31 * result + (data?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "Resource{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}'
    }

    companion object {

        fun <T> success(data: T?, isSyncing: Boolean): Resource<T> {
            return Resource(SUCCESS, data, null, isSyncing)
        }

        fun <T> error(msg: String, data: T?, isSyncing: Boolean): Resource<T> {
            return Resource(ERROR, data, msg, isSyncing)
        }

        fun <T> loading(data: T?, isSyncing: Boolean): Resource<T> {
            return Resource(LOADING, data, null, isSyncing)
        }
    }
}
