package com.example.taylo.trainscheduler.ui.base

import android.support.annotation.StringRes

interface IBaseView {

    fun showLoading(message: String?)

    fun showLoading()

    fun hideLoading()

    fun onError(errorString : String)

    fun onError(@StringRes resId: Int)

    fun networkConnected() : Boolean


}