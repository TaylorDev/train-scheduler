package com.example.taylo.trainscheduler.ui.activity.full_settings

import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.base.IBasePresenter


interface IFullSettingsPresenter: IBasePresenter<IFullSettingsView> {
    fun setStationId(stationId: Long)
    fun onSaveClicked(station: Station): Boolean
    fun refresh()
}