package com.example.taylo.trainscheduler.data

import com.example.taylo.trainscheduler.data.database.DbManager
import com.example.taylo.trainscheduler.data.parser.MyParser
import com.example.taylo.trainscheduler.data.prefs.PrefsManager
import com.example.taylo.trainscheduler.event_bus.SyncEvent
import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.model.station.StationSchedule
import com.example.taylo.trainscheduler.model.station.StationScheduleTest
import com.example.taylo.trainscheduler.model.station.StationTest
import com.example.taylo.trainscheduler.utils.rx.ISchedulerProvider
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class DataManagerImpl
@Inject
constructor(private val parser: MyParser,
            private val schedulerProvider: ISchedulerProvider,
            private val prefs: PrefsManager,
            private val database: DbManager,
            private val assetExtractor: AssetExtractor) : DataManager {

    override fun isSyncing(): Boolean {
        return isTimeTableSyncing
    }

    private var isTimeTableSyncing = false
    private var isTimeTableSyncingWithFirebase = false

    companion object {
        private const val WEEK_MILLIS = 604800000
        private const val SYNC_WITH_FIREBASE_MILLIS = 10000

        private const val FIREBASE_TABLE = "stations"
        const val FIREBASE_TEST_TABLE = "example"
    }

    override fun triggerSync(isForce: Boolean) {
        if (isTimeTableSyncing) {
            return
        }

        Timber.d("triggerSyncc %s", prefs.isFirstLaunch())
        if (prefs.isFirstLaunch()) {
            fillDataBase()
        } else {
            loadFromNetIfNeeded(isForce)
            loadFromFirebaseIfNeeded(isForce)
        }
    }

    override fun getAllTimeTableSingle(filterParam: String, withDetail: Boolean): Single<Resource<MutableList<Station>>> {
        Timber.d("getAllTimeTableSingle")
        loadFromNetIfNeeded()
        loadFromFirebaseIfNeeded()
        return database.findTimeTable()
                .toObservable()
                .flatMap { list -> Observable.fromIterable(list) }
                .filter{ item -> filterParam.isEmpty() || item.nameStation!!.toLowerCase().contains(filterParam.toLowerCase()) }
                .toList()
                .map { list->
                    if (withDetail) {
                        computeDetail(list)
                    }
                    Resource.success(list, isTimeTableSyncing)
                }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
    }

    override fun getFavoritesTimeTableSingle(withDetail: Boolean): Single<Resource<MutableList<Station>>> {
        Timber.d("getFavoritesTimeTableSingle")
        loadFromNetIfNeeded()
        loadFromFirebaseIfNeeded()
        return database.findFavoriteTimeTable()
                .map { list->
                    if (withDetail) {
                        computeDetail(list)
                    }
                    Resource.success(list, isTimeTableSyncing)
                }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
    }

    override fun updateStation(item: Station) {
        Timber.d("updateStation")
        database.updateStation(item)
    }

    private fun fillDataBase() {
        Timber.d("fillDataBase")
        assetExtractor.getList()
                .subscribeOn(schedulerProvider.io())
                .subscribe({ list->
                    Timber.d("fillDataBase success %s", list)
                    prefs.saveFirstLaunch(false)
                    database.insertTimeTable(list)
                    loadFromNetIfNeeded()
                })
    }

    private fun loadFromNetIfNeeded(isForce: Boolean = false) {
        Timber.d("loadFromNetIfNeeded %s", isTimeTableSyncing)
        if (isTimeTableSyncing) {
            return
        }
        isTimeTableSyncing = true
        val currentTime = System.currentTimeMillis()
        val updatedTime = prefs.getLastUpdatedTime()

        if ((currentTime - updatedTime) > WEEK_MILLIS || isForce) {
            database.findTimeTable()
                    .flatMap { list ->
                        Timber.tag("fkgnfjgfj").d("loadFromNetIfNeeded findTimeTable %s", list[0].scheduleList.size)
                        parser.parse(list) }
                    .retryWhen { error -> error.flatMap { t ->
                            Timber.e(t, "loadFromNetIfNeeded retry")
                            Flowable.just("").delay(10, TimeUnit.SECONDS)
                        }
                    }
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(schedulerProvider.ui())
                    .subscribe({list ->
                        Timber.d("loadFromNetIfNeeded success %s", list.size)
                        Timber.tag("fkgnfjgfj").d("loadFromNetIfNeeded updateTimeTable %s", list[0].scheduleList.size)
                        isTimeTableSyncing = false
                        prefs.saveLastUpdatedTime(System.currentTimeMillis())
                        database.updateTimeTable(list)


                    })
        } else {
            isTimeTableSyncing = false
        }
    }

    private fun loadFromFirebaseIfNeeded(isForce: Boolean = false) {
        Timber.d("loadFromFirebaseIfNeeded")
        if (isTimeTableSyncingWithFirebase) {
            return
        }

        val currentTime = System.currentTimeMillis()
        val updatedTime = prefs.getLastUpdatedTimeWithFirebase()

        if ((currentTime - updatedTime) < SYNC_WITH_FIREBASE_MILLIS || isForce) {
            return
        }

        isTimeTableSyncingWithFirebase = true
        val db = FirebaseFirestore.getInstance()
        Timber.d("loadFromFirebaseIfNeeded execute")
        db.collection(FIREBASE_TABLE).get().addOnCompleteListener{ task ->
            isTimeTableSyncingWithFirebase = false

            if (task.isSuccessful) {
                val stationItems = mutableListOf<Station>()
                Timber.d("loadFromFirebaseIfNeeded complete %s", task.result.size())
                for (document in task.result) {
                    val gson = Gson()
                    val jsonElement = gson.toJsonTree(document.data)
                    val stationItem = gson.fromJson(jsonElement, Station::class.java)
                    stationItems.add(stationItem)
                }
                prefs.saveLastUpdatedTimeWithFirebase(System.currentTimeMillis())
                database.updateTimeTableFromFirebase(stationItems)
                EventBus.getDefault().post(SyncEvent(isSyncing()))
            } else {
                Timber.e(task.exception, "loadFromFirebaseIfNeeded")
            }
        }
    }

    override fun getDetailStationSingle(id: Long): Single<Resource<Station>> {
        return database.findStation(id)
                .map { station ->
                    computeDetail(station)
                    Resource.success(station, isTimeTableSyncing)
                }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
    }

    override fun updateFavorites(stations: MutableList<Station>) {
        database.updateFavorites(stations)
    }




    private fun computeDetail(list: MutableList<Station>) {
        for(item in list) {
            StationSchedule.computeDetail(item.scheduleList)
        }
    }

    private fun computeDetail(station: Station) {
        StationSchedule.computeDetail(station.scheduleList)
    }
}