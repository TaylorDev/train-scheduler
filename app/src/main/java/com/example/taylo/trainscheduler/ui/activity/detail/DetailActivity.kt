package com.example.taylo.trainscheduler.ui.activity.detail

import android.app.Activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.example.taylo.trainscheduler.R
import com.example.taylo.trainscheduler.adapter.DetailDelegateAdapter
import com.example.taylo.trainscheduler.adapter.base.CompositeDelegateAdapter
import com.example.taylo.trainscheduler.event_bus.SyncEvent
import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.Status
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_detail.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject
class DetailActivity : BaseActivity(), DetailMvpView {

    @Inject
    lateinit var presenter: DetailPresenter

    lateinit var adapter: CompositeDelegateAdapter

    companion object {

        const val EXTRA_STATION_ID = "EXTRA_STATION_ID"

        fun startForResult(activity: Activity, stationId: Long, requestCode: Int) {
            val intent = Intent(activity, DetailActivity::class.java)
            intent.putExtra(EXTRA_STATION_ID, stationId)
            activity.startActivityForResult(intent, requestCode)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        getActivityComponent().inject(this)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = ""

        adapter = CompositeDelegateAdapter.Builder()
                .add(DetailDelegateAdapter())
                .build()

        list.layoutManager = LinearLayoutManager(this)
        list.adapter = adapter

        swipeRefresh.setOnRefreshListener {
            presenter.refresh()
        }

        presenter.onAttach(this)
        presenter.setStationId(intent.getLongExtra(EXTRA_STATION_ID, 0))
        presenter.refresh()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    override fun onSyncStateChanged(event: SyncEvent) {
        presenter.refresh()
    }

    private fun setFavorite(isFavorite: Boolean) {
        if (isFavorite) {
            favoriteImageView.setColorFilter(ContextCompat.getColor(this, R.color.orange))
        } else {
            favoriteImageView.setColorFilter(ContextCompat.getColor(this, R.color.grey))
        }
    }

    override fun onProvideResource(resource: Resource<Station>) {
        swipeRefresh.isRefreshing = resource.status == Status.LOADING
        if (resource.isSyncing) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
        }

        if (resource.data != null) {
            val stationItem = resource.data
            adapter.swapData(stationItem.scheduleList)

            val mytitle = "ст. " + stationItem.nameStation
            nameTextView.text = mytitle
            title = mytitle.toUpperCase()

            setFavorite(stationItem.isFavorite)

            favoriteImageView.setOnClickListener {
                stationItem.isFavorite = !stationItem.isFavorite
                presenter.setFavorite(stationItem)
                setFavorite(stationItem.isFavorite)
            }

            when(stationItem.findState()) {
                Station.STATE.OPEN -> {
                    isClosedTextView.text = "Сейчас открыта"
                    imageView.setColorFilter(ContextCompat.getColor(this, R.color.green))
                    closeTimeTextView.text = "Закроется через " + stationItem.getNextTime(true)
                }
                Station.STATE.CLOSED -> {
                    isClosedTextView.text = "Сейчас закрыта"
                    imageView.setColorFilter(ContextCompat.getColor(this, R.color.red))
                    closeTimeTextView.text = "Откроется через " + stationItem.getNextTime(false)
                }
                Station.STATE.UNKNOWN -> {
                    isClosedTextView.text = "Нет информации"
                    imageView.setColorFilter(ContextCompat.getColor(this, R.color.black))
                    closeTimeTextView.text = "Нет информации"
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
