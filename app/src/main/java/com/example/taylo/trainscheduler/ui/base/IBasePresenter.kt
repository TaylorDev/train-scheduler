package com.example.taylo.trainscheduler.ui.base

interface IBasePresenter<in V: IBaseView> {

    fun onAttach(view: V)

    fun onDetach()
}