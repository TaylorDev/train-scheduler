package com.example.taylo.trainscheduler.ui.activity.favorit

import com.example.taylo.trainscheduler.data.DataManager
import com.example.taylo.trainscheduler.di.ActivityScope
import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.base.BasePresenter
import com.example.taylo.trainscheduler.utils.rx.ISchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject

@ActivityScope
class FavoritePresenterImpl
@Inject
constructor(schedulerProvider: ISchedulerProvider,
            dataManager: DataManager,
            compositeDisposable: CompositeDisposable) : BasePresenter<IFavoriteView>(schedulerProvider , dataManager, compositeDisposable) , FavoritePresenter {

    override fun onAttach(view: IFavoriteView) {
        super.onAttach(view)
        loadFavorites()
    }

    private fun loadFavorites() {

        mvpView?.onProvideResource(Resource.loading(null, dataManager.isSyncing()))

        compositeDisposable.add(dataManager.getFavoritesTimeTableSingle(true)
                .subscribe({ resource ->
                    Timber.d("loadFavorites success")
                    mvpView?.onProvideResource(resource)
                }, {t ->
                    Timber.e(t, "loadFavorites error")
                }))
    }

    override fun refresh() {
        loadFavorites()
    }

    override fun removeFromFavorites(item: Station) {
        item.isFavorite = false
        dataManager.updateStation(item)
    }
}