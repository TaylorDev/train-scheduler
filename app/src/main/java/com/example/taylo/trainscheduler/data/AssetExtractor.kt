package com.example.taylo.trainscheduler.data

import android.content.Context
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.model.station.StationResponse
import com.google.gson.Gson
import io.reactivex.Single
import java.io.InputStreamReader

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AssetExtractor @Inject
constructor(private val context: Context) {

    fun getList(): Single<MutableList<Station>> {
        return Single.fromCallable {
            val inputRead = InputStreamReader(context.assets.open("station.json"))
            var message = ""
            inputRead.forEachLine {
                message += it
            }
            inputRead.close()
            val data = Gson().fromJson(message, StationResponse::class.java)
            data.station
        }
    }
}
