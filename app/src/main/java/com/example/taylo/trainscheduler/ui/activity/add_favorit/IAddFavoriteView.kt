package com.example.taylo.trainscheduler.ui.activity.add_favorit

import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.base.IBaseView


interface IAddFavoriteView : IBaseView {
    fun onProvideResource(resource: Resource<MutableList<Station>>)
}