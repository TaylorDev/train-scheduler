package com.example.taylo.trainscheduler.adapter

import com.example.taylo.trainscheduler.R
import com.example.taylo.trainscheduler.adapter.base.BaseDelegateAdapter
import com.example.taylo.trainscheduler.adapter.base.KViewHolder
import com.example.taylo.trainscheduler.model.station.Station
import kotlinx.android.synthetic.main.list_item_add_favorite.view.*

class AddFavoriteDelegateAdapter : BaseDelegateAdapter<Station>() {

    override fun isForViewType(items: List<Any>, position: Int): Boolean {
        return items[position] is Station
    }

    override fun onBindViewHolder(holder: KViewHolder, item: Station, position: Int) {
        holder.containerView.checkBox.text = item.nameStation
        holder.containerView.checkBox.isChecked = item.isFavorite
        holder.containerView.checkBox.setOnCheckedChangeListener({ buttonView, isChecked ->
            item.isFavorite = isChecked
        })
    }

    override fun recycled(holder: KViewHolder) {
        super.recycled(holder)
        holder.containerView.checkBox.setOnCheckedChangeListener(null)
    }

    override fun getLayoutId(): Int {
        return R.layout.list_item_add_favorite
    }
}
