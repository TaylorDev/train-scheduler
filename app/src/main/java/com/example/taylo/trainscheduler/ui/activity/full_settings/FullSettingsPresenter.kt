package com.example.taylo.trainscheduler.ui.activity.full_settings

import com.example.taylo.trainscheduler.data.DataManager
import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.notifications.ReminderManager
import com.example.taylo.trainscheduler.ui.base.BasePresenter
import com.example.taylo.trainscheduler.utils.rx.ISchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject


class FullSettingsPresenter
@Inject
constructor(schedulerProvider: ISchedulerProvider,
            dataManager: DataManager,
            compositeDisposable: CompositeDisposable,
            val reminderManager: ReminderManager) : BasePresenter<IFullSettingsView>(schedulerProvider , dataManager, compositeDisposable) , IFullSettingsPresenter {

    private var stationId: Long = 0

    override fun setStationId(stationId: Long) {
        this.stationId = stationId
    }

    override fun onAttach(view: IFullSettingsView) {
        super.onAttach(view)
        loadData()
    }

    private fun loadData() {
        mvpView?.onProvideResource(Resource.loading(null, dataManager.isSyncing()))

        compositeDisposable.add(dataManager.getDetailStationSingle(stationId)
                .subscribe({ resource ->
                    Timber.d("loadData success")
                    mvpView?.onProvideResource(resource)
                }, {t ->
                    Timber.e(t, "loadData error")
                }))
    }

    override fun onSaveClicked(station: Station): Boolean {
        dataManager.updateStation(station)
        return reminderManager.setReminder(station)
    }

    override fun refresh() {
        loadData()
    }
}