package com.example.taylo.trainscheduler.data.database

import com.example.taylo.trainscheduler.model.station.Station

import io.reactivex.Single

interface DbManager {

    fun findTimeTable(): Single<MutableList<Station>>

    fun findFavoriteTimeTable(): Single<MutableList<Station>>

    fun updateStation(item: Station)

    fun insertTimeTable(list: MutableList<Station>)

    fun updateTimeTable(list: MutableList<Station>)

    fun updateTimeTableFromFirebase(newlist: MutableList<Station>)

    fun findStation(id: Long): Single<Station>

    fun updateFavorites(stations: MutableList<Station>)

}
