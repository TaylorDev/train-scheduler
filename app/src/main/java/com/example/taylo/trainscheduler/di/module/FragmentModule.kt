package com.example.taylo.trainscheduler.di.module

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import com.example.taylo.trainscheduler.di.FragmentScope
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
class FragmentModule(private val fragment: Fragment) {

    @Provides
    @FragmentScope
    fun provideContext():
            Context? {
        return fragment.context
    }


    @Provides
    @FragmentScope
    fun provideActivity():
            FragmentActivity? {
        return fragment.activity
    }

    @Provides
    @FragmentScope
    fun provideFragment():
            Fragment {
        return fragment
    }

    @Provides
    @FragmentScope
    fun provideCompositeDisposable():
            CompositeDisposable {
        return CompositeDisposable()
    }
}