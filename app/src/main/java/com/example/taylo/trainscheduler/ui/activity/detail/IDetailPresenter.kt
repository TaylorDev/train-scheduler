package com.example.taylo.trainscheduler.ui.activity.detail

import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.base.IBasePresenter

interface IDetailPresenter: IBasePresenter<DetailMvpView> {
    fun refresh()
    fun setStationId(id: Long)
    fun setFavorite(station: Station)
}