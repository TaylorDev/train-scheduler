package com.example.taylo.trainscheduler.ui.activity.full_settings

import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.base.IBaseView


interface IFullSettingsView: IBaseView {
    fun onProvideResource(resource: Resource<Station>)
}