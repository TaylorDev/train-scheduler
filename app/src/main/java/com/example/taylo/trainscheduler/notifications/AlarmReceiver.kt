package com.example.taylo.trainscheduler.notifications

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.support.v4.app.NotificationCompat
import com.example.taylo.trainscheduler.R
import com.example.taylo.trainscheduler.app.MyApplication
import com.example.taylo.trainscheduler.data.DataManager
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.activity.detail.DetailActivity
import com.example.taylo.trainscheduler.utils.TimeUtils
import timber.log.Timber
import javax.inject.Inject

class AlarmReceiver : BroadcastReceiver() {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var reminderManager: ReminderManager

    companion object {
        var STATION_ITEM_ID = "STATION_ITEM_ID"
    }

    override fun onReceive(context: Context, intent: Intent) {
        MyApplication.appComponent.inject(this)

        val stationItemId = intent.getLongExtra(STATION_ITEM_ID, -1)

        dataManager.getDetailStationSingle(stationItemId)
                .subscribe({ resource->
                    val stationItem = resource.data
                    if (stationItem != null) {
                        showNotification(context, stationItem)
                        setNextAlarmIfNeeded(stationItem)
                    }
                }, { t->
                    Timber.e(t)
                })
    }

    private fun showNotification(context: Context, station: Station) {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val contentText = if (station.beforeNotificationTimeMinutes == 0) {
            "закроется сейчас"
        } else {
            "закроется через " + TimeUtils.getFormattedStr(station.beforeNotificationTimeMinutes) + " мин"
        }

        val builder = NotificationCompat.Builder(context)
                .setContentTitle(station.nameStation)
                .setContentText(contentText)
                .setSmallIcon(R.drawable.ic_notifications_black_24dp)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))

        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra(DetailActivity.EXTRA_STATION_ID, station.id)
        val activity = PendingIntent.getActivity(context, 1, intent, PendingIntent.FLAG_CANCEL_CURRENT)
        builder.setContentIntent(activity)

        val notification = builder.build()

        notificationManager.notify(station.id.toInt(), notification)
    }

    private fun setNextAlarmIfNeeded(station: Station) {
        dataManager.getDetailStationSingle(station.id)
                .subscribe({ resource ->
                    Timber.d("success")
                    if (resource.data != null) {
                        reminderManager.setReminder(resource.data)
                    }
                }, { t ->
                    Timber.e(t)
                })
    }
}