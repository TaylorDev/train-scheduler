package com.example.taylo.trainscheduler.ui.activity.add_favorit

import com.example.taylo.trainscheduler.data.DataManager
import com.example.taylo.trainscheduler.model.Resource
import com.example.taylo.trainscheduler.model.station.Station
import com.example.taylo.trainscheduler.ui.base.BasePresenter
import com.example.taylo.trainscheduler.utils.rx.ISchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject

class AddFavoritePresenter
@Inject
constructor(schedulerProvider: ISchedulerProvider, dataManager: DataManager, compositeDisposable: CompositeDisposable)
    : BasePresenter<IAddFavoriteView>(schedulerProvider , dataManager, compositeDisposable) , IAddFavoritePresenter {


    private var filterParam = ""

    override fun onAttach(view: IAddFavoriteView) {
        super.onAttach(view)
        loadTimeTable()
    }

    fun search(filterParam: String) {
        this.filterParam = filterParam
        loadTimeTable()
    }

    private fun loadTimeTable() {

        mvpView?.onProvideResource(Resource.loading(null, dataManager.isSyncing()))

        compositeDisposable.add(dataManager.getAllTimeTableSingle(filterParam, false)
                .subscribe({ resource ->
                    Timber.d("loadTimeTable success")
                    mvpView?.onProvideResource(resource)
                }, { t ->
                    Timber.e(t, "my error")
                }))
    }

    override fun save(stations: MutableList<Station>) {
        dataManager.updateFavorites(stations)
    }

    override fun refresh() {
        loadTimeTable()
    }
}