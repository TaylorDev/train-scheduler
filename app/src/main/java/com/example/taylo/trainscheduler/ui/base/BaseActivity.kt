package com.example.taylo.trainscheduler.ui.base

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.inputmethod.InputMethodManager
import com.example.taylo.trainscheduler.app.MyApplication
import com.example.taylo.trainscheduler.di.component.ActivityComponent
import com.example.taylo.trainscheduler.di.component.DaggerActivityComponent
import com.example.taylo.trainscheduler.di.module.ActivityModule
import com.example.taylo.trainscheduler.event_bus.SyncEvent
import com.example.taylo.trainscheduler.utils.NetworkUtil
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import timber.log.Timber


abstract class BaseActivity: AppCompatActivity(), IBaseView {

    private lateinit var activityComponent : ActivityComponent

    private var progress: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventBus.getDefault().register(this)
        activityComponent = DaggerActivityComponent.builder()
                .activityModule(ActivityModule(this))
                .applicationComponent((application as MyApplication).getComponent())
                .build()
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    abstract fun onSyncStateChanged(event: SyncEvent)

    fun getActivityComponent(): ActivityComponent {
        return activityComponent
    }

    override fun onError(errorString: String) {
        Timber.d("onError %s", errorString)
    }

    override fun onError(resId: Int) {
        Timber.d("onError %s", getString(resId))
    }

    override fun showLoading(message: String?) {
        hideLoading()
        progress = ProgressDialog(this)
        progress!!.setMessage(message)
        progress!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        //progress!!.setCancelable(false)
        Timber.d("showLoading")
        progress!!.show()
    }

    override fun showLoading() {
        showLoading(null)
    }

    override fun hideLoading() {
        progress?.dismiss()
        progress = null
        Timber.d("hideLoading")
    }

    override fun networkConnected(): Boolean {
        Timber.d("networkConnected")
        return NetworkUtil.networkConnected(applicationContext)
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun onFragmentAttached() {

    }

    fun openActivityOnTokenExpire() {

    }
}