package com.example.taylo.trainscheduler.adapter

import com.example.taylo.trainscheduler.R
import com.example.taylo.trainscheduler.adapter.base.BaseDelegateAdapter
import com.example.taylo.trainscheduler.adapter.base.KViewHolder
import com.example.taylo.trainscheduler.model.station.Station
import kotlinx.android.synthetic.main.list_item_settings.view.*

class SettingsDelegateAdapter(private val callback: Callback) : BaseDelegateAdapter<Station>() {

    override fun isForViewType(items: List<Any>, position: Int): Boolean {
        return items[position] is Station
    }

    override fun onBindViewHolder(holder: KViewHolder, item: Station, position: Int) {
        holder.containerView.nameTextView.text = item.nameStation
        holder.containerView.nameTextView.setOnClickListener({
            callback.onItemClicked(item)
        })
    }

    override fun getLayoutId(): Int {
        return R.layout.list_item_settings
    }

    interface Callback {
        fun onItemClicked(item: Station)
    }
}
