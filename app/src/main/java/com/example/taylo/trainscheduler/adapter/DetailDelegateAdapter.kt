package com.example.taylo.trainscheduler.adapter

import com.example.taylo.trainscheduler.R
import com.example.taylo.trainscheduler.adapter.base.BaseDelegateAdapter
import com.example.taylo.trainscheduler.adapter.base.KViewHolder
import com.example.taylo.trainscheduler.model.station.StationSchedule
import kotlinx.android.synthetic.main.list_item_detail.view.*

class DetailDelegateAdapter : BaseDelegateAdapter<StationSchedule>() {

    override fun isForViewType(items: List<Any>, position: Int): Boolean {
        return items[position] is StationSchedule
    }

    override fun onBindViewHolder(holder: KViewHolder, item: StationSchedule, position: Int) {
        holder.containerView.text.text = item.computeTimeStringInterval()
    }

    override fun getLayoutId(): Int {
        return R.layout.list_item_detail
    }
}