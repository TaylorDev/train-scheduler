package com.example.taylo.trainscheduler.model.station

import com.example.taylo.trainscheduler.model.ScheduleListConverter
import com.example.taylo.trainscheduler.utils.TimeUtils
import com.example.taylo.trainscheduler.utils.TimeUtils.getCalendarLocal
import com.example.taylo.trainscheduler.utils.TimeUtils.getCurrentTimeByMoscow
import com.google.gson.annotations.SerializedName
import io.objectbox.annotation.Convert
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import java.io.Serializable
import java.util.*

@Entity
data class Station(
		@Id
		var id: Long = 0,

		var nameStation: String? = null,

		var lat: Double? = null,

		var lng: Double? = null,

		var url: String? = null,

		var isFavorite: Boolean = false,

		@Convert(converter = ScheduleListConverter::class, dbType = String::class)
		var scheduleList: MutableList<StationSchedule> = ArrayList(),

		var hasNotification: Boolean = false,

		var beforeNotificationTimeMinutes: Int = 0,

		var isAlwaysNotify: Boolean = true,

		var notifyFromHour: Int = 0,

		var notifyFromMinute: Int = 0,

		var notifyUntilHour: Int = 0,

		var notifyUntilMinute: Int = 0): Serializable {

	fun findState(): STATE {
		if(scheduleList.isEmpty()) {
			return STATE.UNKNOWN
		}

		val calendar = Calendar.getInstance()
		val currentTimeMillis = calendar.timeInMillis

		for (schedule in scheduleList) {
			if (currentTimeMillis >= schedule.beforeCloseTimeMillis &&
					currentTimeMillis <= schedule.afterCloseTimeMillis) {
				return STATE.CLOSED
			}
		}

		return STATE.OPEN
	}

	fun getNextTime(isOpen: Boolean): String {
		var time = "-//-"

		val currentTimeMillis = getCurrentTimeByMoscow()

		val nextTimeMillis = if (isOpen) {
			getNextCloseTime()
		} else {
			getNextOpenTime()
		}

		if (nextTimeMillis != -1L) {
			val remainingTime = nextTimeMillis - currentTimeMillis
			time = TimeUtils.getStringTime(remainingTime, true)
		}

		return time
	}

	private fun getNextCloseTime(): Long {
		val currentTimeMillis = getCurrentTimeByMoscow()
		for (schedule in scheduleList) {
			if (currentTimeMillis <= (schedule.beforeCloseTimeMillis)) {
				return schedule.beforeCloseTimeMillis
			}
		}

		return -1L
	}

	private fun getNextOpenTime(): Long {
		val currentTimeMillis = getCurrentTimeByMoscow()
		for (schedule in scheduleList) {
			if (currentTimeMillis <= (schedule.afterCloseTimeMillis)) {
				return schedule.afterCloseTimeMillis
			}
		}

		return -1L
	}

	fun getNextCloseTimeForAlarm(): Long? {
		val calendar = getCalendarLocal()
		val currentTimeMillis = calendar.timeInMillis
		val extraBeforeMillis = beforeNotificationTimeMinutes * 60 * 1000

		val notifyFromMinutes = notifyFromHour * 60 + notifyFromMinute
		val notifyUntilMinutes = notifyUntilHour * 60 + notifyUntilMinute

		for (schedule in scheduleList) {
			val alarmTimeMillis = schedule.beforeCloseTimeMillis - extraBeforeMillis
			calendar.timeInMillis = alarmTimeMillis
			if (currentTimeMillis <= alarmTimeMillis) {
				val alarmHour = calendar.get(Calendar.HOUR_OF_DAY)
				val alarmMinute = calendar.get(Calendar.MINUTE)
				val alarmMinutes = alarmHour * 60 + alarmMinute
				if (isAlwaysNotify || (!isAlwaysNotify && alarmMinutes >= notifyFromMinutes && alarmMinutes <= notifyUntilMinutes)) {
					return schedule.beforeCloseTimeMillis - extraBeforeMillis
				}
			}
		}

		return null
	}

	enum class STATE {
		OPEN, CLOSED, UNKNOWN
	}
}