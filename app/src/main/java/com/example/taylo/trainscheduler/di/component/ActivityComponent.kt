package com.example.taylo.trainscheduler.di.component

import com.example.taylo.trainscheduler.ui.activity.main.MainActivity
import com.example.taylo.trainscheduler.di.ActivityScope
import com.example.taylo.trainscheduler.di.module.ActivityModule
import com.example.taylo.trainscheduler.ui.activity.add_favorit.AddFavoriteActivity
import com.example.taylo.trainscheduler.ui.activity.detail.DetailActivity
import com.example.taylo.trainscheduler.ui.activity.favorit.FavoriteActivity
import com.example.taylo.trainscheduler.ui.activity.full_settings.FullSettingsActivity
import com.example.taylo.trainscheduler.ui.activity.search.SearchActivity
import com.example.taylo.trainscheduler.ui.activity.search.dialog.DetailDialog
import com.example.taylo.trainscheduler.ui.activity.setting.SettingActivity
import dagger.Component

@ActivityScope
@Component(dependencies = [(ApplicationComponent::class)], modules = [(ActivityModule::class)])
interface ActivityComponent {

    fun inject(activity: MainActivity)

    fun inject(activity: SettingActivity)

    fun inject(activity: FavoriteActivity)

    fun inject(activity: AddFavoriteActivity)

    fun inject(activity: FullSettingsActivity)

    fun inject(activity: DetailActivity)

    fun inject(activity: SearchActivity)

    fun inject(dialog: DetailDialog)

}