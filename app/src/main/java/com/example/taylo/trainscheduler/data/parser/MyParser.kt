package com.example.taylo.trainscheduler.data.parser

import com.example.taylo.trainscheduler.model.station.Station
import io.reactivex.Single

interface MyParser {

    fun parse(list: MutableList<Station>): Single<MutableList<Station>>
}
