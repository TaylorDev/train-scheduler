package com.example.taylo.trainscheduler.ui.activity.search

import com.example.taylo.trainscheduler.ui.base.IBasePresenter

interface ISearchPresenter : IBasePresenter<SearchMvpView> {
    fun refresh()
}
