package com.example.taylo.trainscheduler.ui.activity.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.example.taylo.trainscheduler.R
import com.example.taylo.trainscheduler.event_bus.SyncEvent
import com.example.taylo.trainscheduler.ui.activity.about.AboutActivity
import com.example.taylo.trainscheduler.ui.activity.about.AboutAppActivity
import com.example.taylo.trainscheduler.ui.activity.favorit.FavoriteActivity
import com.example.taylo.trainscheduler.ui.activity.search.SearchActivity
import com.example.taylo.trainscheduler.ui.activity.setting.SettingActivity
import com.example.taylo.trainscheduler.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

class MainActivity : BaseActivity(), IMainView {

    @Inject
    lateinit var presenter : MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getActivityComponent().inject(this)

        btn_search.setOnClickListener({
            SearchActivity.start(this)
        })

        btn_favorit.setOnClickListener({
            FavoriteActivity.start(this)
        })

        btn_setting.setOnClickListener({
            startActivity(SettingActivity.getStartIntent(this))
        })

        about_train.setOnClickListener {
            startActivity(Intent(this, AboutActivity::class.java))
        }

        btn_about.setOnClickListener {
            startActivity(Intent(this, AboutAppActivity::class.java))
        }

        presenter.onAttach(this)

        Glide.with(this)
                .load(R.drawable.bg)
                .into(bgImageView)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun showSyncing(syncing: Boolean) {
        if (syncing) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    override fun onSyncStateChanged(event: SyncEvent) {
        if (event.isSyncing) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
        }
    }
}
