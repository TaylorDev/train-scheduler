package com.example.taylo.trainscheduler.adapter.base

import android.support.annotation.LayoutRes
import android.view.ViewGroup

interface IDelegateAdapter<in T> {

    fun createViewHolder(parent: ViewGroup, viewType: Int): KViewHolder

    fun bindViewHolder(holder: KViewHolder, item: Any, position: Int)

    fun recycled(holder: KViewHolder)

    fun isForViewType(items: List<Any>, position: Int): Boolean




    fun onBindViewHolder(holder: KViewHolder, item: T, position: Int)

    @LayoutRes
    fun getLayoutId(): Int
}