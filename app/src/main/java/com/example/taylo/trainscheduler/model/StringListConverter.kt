package com.example.taylo.trainscheduler.model

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import io.objectbox.converter.PropertyConverter

class StringListConverter : PropertyConverter<MutableList<String>, String> {

    private val gson = Gson()
    private val itemsMapType = object : TypeToken<MutableList<String>>() {}.type

    override fun convertToEntityProperty(databaseValue: String?): MutableList<String>? {
        return if (databaseValue == null) {
            null
        } else {
            gson.fromJson<MutableList<String>>(databaseValue, itemsMapType)
        }
    }

    override fun convertToDatabaseValue(entityProperty: MutableList<String>): String {
        return Gson().toJson(entityProperty)
    }
}