package com.example.taylo.trainscheduler.model

import com.example.taylo.trainscheduler.model.station.StationSchedule
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.objectbox.converter.PropertyConverter
import timber.log.Timber

class ScheduleListConverter : PropertyConverter<MutableList<StationSchedule>, String> {

    private val gson = Gson()
    private val itemsMapType = object : TypeToken<MutableList<StationSchedule>>() {}.type

    override fun convertToEntityProperty(databaseValue: String?): MutableList<StationSchedule>? {
        return if (databaseValue == null) {
            null
        } else {
            var list: MutableList<StationSchedule>? = null
            try {
                list = gson.fromJson<MutableList<StationSchedule>>(databaseValue, itemsMapType)
            } catch (exc: Exception) {
                Timber.e(exc, "convertToEntityProperty error")
            }

            list
        }
    }

    override fun convertToDatabaseValue(entityProperty: MutableList<StationSchedule>): String {
        return Gson().toJson(entityProperty)
    }

}
